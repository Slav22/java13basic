import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double x = scanner.nextDouble();

        System.out.println((Math.round(Math.sin(x) * Math.sin(x) + Math.cos(x) * Math.cos(x) - 1)) == 0);
    }
}
