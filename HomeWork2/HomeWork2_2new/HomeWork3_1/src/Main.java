import Oop.*;

import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        //Тестирование Student
        Student student = new Student();
        student.setName("Андрей");
        student.setSurname("Иванов");
        System.out.println("ФИО: " + student.getName() + " " + student.getSurname());
        student.pushGrades(4);
        student.pushGrades(5);
        student.pushGrades(4);

        student.pushGrades(3);
        student.pushGrades(3);
        student.pushGrades(3);
        student.pushGrades(3);

        student.pushGrades(5);
        student.pushGrades(4);
        student.pushGrades(5);


        student.pushGrades(5);
        student.pushGrades(5);
        student.pushGrades(3);
        System.out.println("Average: " + student.average());


        //Тестирование TimeUnit
        TimeUnit timeUnit = new TimeUnit(0, 50, 20);
        timeUnit.formatOutAsia();
        timeUnit.addTime(3, 40, 55);
        timeUnit.formatOutUsa();


        //Тестирование DayOfWeek

        DayOfWeek[] days = new DayOfWeek[7];
        days[0] = new DayOfWeek((byte) 1, "Monday");
        days[1] = new DayOfWeek((byte) 2, "Tuesday");
        days[2] = new DayOfWeek((byte) 3, "Wednesday");
        days[3] = new DayOfWeek((byte) 4, "Thursday");
        days[4] = new DayOfWeek((byte) 5, "Friday");
        days[5] = new DayOfWeek((byte) 6, "Saturday");
        days[6] = new DayOfWeek((byte) 7, "Sunday");
        for (DayOfWeek day : days) {
            System.out.println(day.getDayNum() + "  " + day.getName());
        }

        //Тестирование AmazingString
        AmazingString string = new AmazingString("   Hello Word");
        System.out.println("Length: " + string.getSize());
        System.out.println("4 -символ=" + string.charAt(4));
        string.print();//Вывод
        System.out.println("sub str Word=" + string.isSubStr("World"));
        string.ltrim();
        string.reverse();

        string.print();

        //Тестирование TriangleChecker
        System.out.println("check(4,6,7)->" + TriangleChecker.check(4, 6, 7));
        System.out.println("check(7,8,10)->" + TriangleChecker.check(7, 8, 10));
        System.out.println("check(1,2,3)->" + TriangleChecker.check(1, 2, 3));
        System.out.println("check(4,2,4)->" + TriangleChecker.check(4, 2, 4));
        System.out.println("check(0,16,17)->" + TriangleChecker.check(0, 16, 17));


        //Тестирование Atm
        Atm sber = new Atm(61, 0.0104);
        Atm gazprom = new Atm(58, 0.0106);
        System.out.println("Instance count: " + Atm.getInstanceCnt());
        System.out.println("450$ to Rub in Sber Atm: " + sber.toRub(450));
        System.out.println("12000Rub to USD in Gazprom Atm: " + gazprom.toUSD(12000));


        // Тестируем StudentService

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество студентов: ");
        int n = scanner.nextInt();//размер массива
        StudentOld[] students = new StudentOld[n];
        for (int i = 0; i < n; i++) {
            students[i] = new StudentOld();
            students[i].Input(scanner);
        }
        StudentService service = new StudentService();
        StudentOld best = service.bestStudent(students);
        System.out.println("Лучший студент: ");
        best.Output();
        service.sortBySurname(students);
        for (int i = 0; i < n; i++)
            students[i].Output();
    }


}

//класс Student Service
class StudentService {
    //Реализуем функцию нахождение максимального по среднему балу
    public StudentOld bestStudent(StudentOld[] students) {
        //Возвращает лучшего
        double maxGrad = -1.0;
        int ind_mx = 0;
        for (int i = 0; i < students.length; i++) {
            if (students[i].average() > maxGrad) {
                maxGrad = students[i].average();
                ind_mx = i;
            }
        }
        return students[ind_mx];
    }

    public void sortBySurname(StudentOld[] students) {
        System.out.println("Сортировка по фамилии ");
        for (int left = 0; left < students.length; left++) {
            int minInd = left;
            for (int i = left; i < students.length; i++) {
                if (students[i].getSurname().compareTo(students[minInd].getSurname()) < 0) {
                    minInd = i;
                }
            }
            StudentOld tmp = students[left];
            students[left] = students[minInd];
            students[minInd] = tmp;
        }
    }
}



