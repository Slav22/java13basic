package Oop;
/* Необходимо реализовать класс AmazingString, который хранит внутри себя
        строку как массив char и предоставляет следующий функционал:
        Конструкторы:
        ● Создание AmazingString, принимая на вход массив char
        ● Создание AmazingString, принимая на вход String
        Публичные методы (названия методов, входные и выходные параметры
        продумать самостоятельно). Все методы ниже нужно реализовать “руками”, т.е.
        не прибегая к переводу массива char в String и без использования стандартных
        методов класса String.
        ● Вернуть i-ый символ строки
        ● Вернуть длину строки
        ● Вывести строку на экран
        ● Проверить, есть ли переданная подстрока в AmazingString (на вход
        подается массив char). Вернуть true, если найдена и false иначе
        ● Проверить, есть ли переданная подстрока в AmazingString (на вход
        подается String). Вернуть true, если найдена и false иначе
        ● Удалить из строки AmazingString ведущие пробельные символы, если
        они есть
        ● Развернуть строку (первый символ должен стать последним, а
        последний первым и т.д.)

 */
public class AmazingString {


    private char[] data;//массив символов
    private int size;//Количество символов

    public AmazingString(String s) {
        size = s.length();
        this.data = new char[size];
        //Инициализации
        for (int i = 0; i < s.length(); i++)
            data[i] = s.charAt(i);

    }

    public AmazingString(char[] str) {
        size = str.length;
        this.data = new char[size];
        for (int i = 0; i < size; i++)
            data[i] = str[i];
    }

    //Вернет длину строк
    public int getSize() {
        return size;
    }

    //Вернет i символ
    public char charAt(int i) {
        return data[i];
    }

    //Вывод строку на экран
    public void print() {
        System.out.println(data);
    }

    //Подстрока
    public boolean isSubStr(char[] sub) {
        if (sub.length <= size) {
            //если количество элементов меньше
            for (int i = 0; i < size; i++) {
                int j = i;
                int cnt = 0;
                int dj = 0;
                while (j < size && data[j] == sub[dj]) {
                    j++;
                    dj++;
                    cnt++;
                }
                cnt++;
                if (cnt == sub.length - 1)
                    return true;
            }
            return false;
        }
        return false;
    }

    public boolean isSubStr(String str) {
        char[] ar = str.toCharArray();
        //используем предыдущего метода
        return isSubStr(ar);
    }

    //Удаление ведущих пробелов
    public void ltrim() {
        int i = 0;
        while (data[i] == ' ') {
            //Пока пробел сдвинем массив символов
            for (int j = 0; j < size - 1; j++) {
                data[j] = data[j + 1];
            }
            size--;
        }
    }

    //развернут
    public void reverse() {
        int l = 0;
        int r = size - 1;
        while (l < r) {
            char tmp = data[l];
            data[l] = data[r];
            data[r] = tmp;
            l++;
            r--;
        }
    }
}



