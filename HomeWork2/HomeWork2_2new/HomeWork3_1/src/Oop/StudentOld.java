package Oop;
/*3. Необходимо реализовать класс StudentService.
        У класса должны быть реализованы следующие публичные методы:
        ● bestStudent() — принимает массив студентов (класс Student из
        предыдущего задания), возвращает лучшего студента (т.е. который
        имеет самый высокий средний балл). Если таких несколько — вывести
        любого.
        ● sortBySurname() — принимает массив студентов (класс Student из
        предыдущего задания) и сортирует его по фамилии.

 */
import java.util.Scanner;

public class StudentOld {
    /*-------------
    Класс студента
    из предыдушего------------------

     */


    private String name;//Имя студента
    private String surname;//Фамилия студента
    private int[] grades;//последние 10 оценок
    private int curGrades;//Количество текущих оценок

    public StudentOld() {
        //Конструктор
        grades = new int[10];//последние 10 оценок
        curGrades = 0;
    }

    //Геттеры
    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int[] getGrades() {
        return grades;
    }
    //Сеттеры

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    //Метод добавление оценку
    public void pushGrades(int grade) {
        //С начало удаляем первую
        for (int i = 0; i < curGrades - 1; i++)
            grades[i] = grades[i + 1];
        grades[curGrades] = grade;
        if (curGrades < 9)
            curGrades++;
    }

    //Метод средний балл
    public double average() {
        int sum = 0;
        for (int i = 0; i < curGrades; i++)
            sum += grades[i];
        double ans = (sum * 1.0) / curGrades;
        return ans;
    }

    //Добавим метод получение данных
    public void Input(Scanner scanner) {
        System.out.println("Введите имя: ");
        name = scanner.next();
        System.out.println("Введите фамилию: ");
        surname = scanner.next();
        int cnt;
        System.out.println("Введите количество оценок: ");
        cnt = scanner.nextInt();
        System.out.println("Введите сами оценкы: ");
        for (int i = 0; i < cnt; i++) {
            int nm = scanner.nextInt();
            pushGrades(nm);
        }
    }

    //Вывод
    public void Output() {
        System.out.print(" | " + surname + " | " + name);
        for (int i = 0; i < curGrades; i++) {
            System.out.print(" "+ grades[i]);
        }
        System.out.println();
    }
}




