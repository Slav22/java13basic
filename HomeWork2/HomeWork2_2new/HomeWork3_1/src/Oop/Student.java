package Oop;

import java.util.Scanner;

/*2. Необходимо реализовать класс Student.
У класса должны быть следующие приватные поля:
● String name — имя студента
● String surname — фамилия студента
● int[] grades — последние 10 оценок студента. Их может быть меньше, но
не может быть больше 10.
И следующие публичные методы:
● геттер/сеттер для name
● геттер/сеттер для surname
● геттер/сеттер для grades
● метод, добавляющий новую оценку в grades. Самая первая оценка
должна быть удалена, новая должна сохраниться в конце массива (т.е.
массив должен сдвинуться на 1 влево).
● метод, возвращающий средний балл студента (рассчитывается как
среднее арифметическое от всех оценок в массиве grades)

 */
public class Student {


//-------------Класс Студент---------------

    private String name;//Имя студента
    private String surname;//Фамилия студента
    private int[] grades;//последние 10 оценок
    private int curGrades;//Количество текущих оценок

    public Student() {
        //Конструктор
        grades = new int[10];//последние 10 оценок
        curGrades = 0;
    }

    //Геттеры
    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int[] getGrades() {
        return grades;
    }
    //Сеттеры

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }
    //Метод добавление оценки

    public void pushGrades(int grade) {
        // удаляем первую
        for (int i = 0; i < curGrades - 1; i++)
            grades[i] = grades[i + 1];
        grades[curGrades] = grade;
        if (curGrades < 9)
            curGrades++;
    }

    //Метод средний балл
    public double average() {
        int sum = 0;
        for (int i = 0; i < curGrades; i++)
            sum += grades[i];
        double ans = (sum * 1.0) / curGrades;
        return ans;
    }


}