package Oop;
/*8. Реализовать класс “банкомат” Atm.
Класс должен:
● Содержать конструктор, позволяющий задать курс валют перевода
долларов в рубли и курс валют перевода рублей в доллары (можно
выбрать и задать любые положительные значения)
● Содержать два публичных метода, которые позволяют переводить
переданную сумму рублей в доллары и долларов в рубли
● Хранить приватную переменную счетчик — количество созданных
инстансов класса Atm и публичный метод, возвращающий этот счетчик
(подсказка: реализуется через static

 */


public class Atm {
    private static int counter=0;
    private  double rubToUSD;
    private double usdToRub;

    public Atm(double rbUs,double UsRb)
    {
        rubToUSD=rbUs;
        usdToRub=UsRb;
        counter++;
    }
    public double toUSD(double rub)
    {
        return rub*usdToRub;
    }
    public double toRub(double usd)
    {
        return rubToUSD*usd;
    }
    //Только сеттеры
    //Что бы кто-нибудь смог посмотреть курс валют
    public double getRubToUSD() {
        return rubToUSD;
    }

    public double getUsdToRub() {
        return usdToRub;
    }
    //Метод возвращаюший количество инстансов
    public static int getInstanceCnt()
    {
        return counter;
    }

}
