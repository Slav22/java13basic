package Oop;
/*3. Необходимо реализовать класс StudentService.
У класса должны быть реализованы следующие публичные методы:
● bestStudent() — принимает массив студентов (класс Student из
предыдущего задания), возвращает лучшего студента (т.е. который
имеет самый высокий средний балл). Если таких несколько — вывести
любого.
● sortBySurname() — принимает массив студентов (класс Student из
предыдущего задания) и сортирует его по фамилии.
 */

import java.util.Scanner;

public class StudentService {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество студентов: ");
        int n = scanner.nextInt();//размер массива
        student[] students = new student[n];
        for (int i = 0; i < n; i++) {
            students[i] = new student();
            students[i].Input(scanner);
        }
        StudentService service = new StudentService();
        student best = service.bestStudent(students);
        System.out.println("Лучший студент: ");
        best.Output();
        service.sortBySurname(students);
        for (int i = 0; i < n; i++)
            students[i].Output();
    }

//класс Student Service
//class StudentService

    //Реализуем функцию нахождение максимального по среднему балу
    public student bestStudent(student[] students) {
        //Возвращать лучший
        double maxGrad = -1.0;
        int ind_mx = 0;
        for (int i = 0; i < students.length; i++) {
            if (students[i].average() > maxGrad) {
                maxGrad = students[i].average();
                ind_mx = i;
            }
        }
        return students[ind_mx];
    }

    public void sortBySurname(student[] students) {
        for (int left = 0; left < students.length; left++) {
            int minInd = left;
            for (int i = left; i < students.length; i++) {
                if (students[i].getSurname().compareTo(students[minInd].getSurname()) < 0) {
                    minInd = i;
                }
            }
            student tmp = students[left];
            students[left] = students[minInd];
            students[minInd] = tmp;
        }
    }
}


//-------------Класс студента с предыдушего------------------
class student {
    private String name;//Имя студента
    private String surname;//Фамилия студента
    private int[] grades;//последние 10 оценок
    private int curGrades;//Количество текущих оценок

    public student() {
        //Конструктор
        grades = new int[10];//последние 10 оценок
        curGrades = 0;
    }

    //Геттеры
    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int[] getGrades() {
        return grades;
    }
    //Сеттеры

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    //Метод добавление оценки
    public void pushGrades(int grade) {
        //С начала удаляем первую
        for (int i = 0; i < curGrades - 1; i++)
            grades[i] = grades[i + 1];
        grades[curGrades] = grade;
        if (curGrades < 9)
            curGrades++;
    }

    //Метод средний балл
    public double average() {
        int sum = 0;
        for (int i = 0; i < curGrades; i++)
            sum += grades[i];
        double ans = (sum * 1.0) / curGrades;
        return ans;
    }

    //Добавим метод получение данных
    public void Input(Scanner scanner) {
        System.out.println("Введите имя: ");
        name = scanner.next();
        System.out.println("Введите фамилию: ");
        surname = scanner.next();
        int cnt;
        System.out.println("Введите количество оценок: ");
        cnt = scanner.nextInt();
        System.out.println("Введите сами оценкы: ");
        for (int i = 0; i < cnt; i++) {
            int nm = scanner.nextInt();
            pushGrades(nm);
        }
    }

    //Вывод
    public void Output() {
        System.out.print("| " + surname + " | " + name);
        for (int i = 0; i < curGrades; i++)
            System.out.print(grades[i]);
        System.out.println();
    }
}


