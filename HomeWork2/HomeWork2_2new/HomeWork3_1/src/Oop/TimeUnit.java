package Oop;

/*4. Необходимо реализовать класс TimeUnit с функционалом, описанным ниже
        (необходимые поля продумать самостоятельно). Обязательно должны быть
        реализованы валидации на входные параметры.
        Конструкторы:
        ● Возможность создать TimeUnit, задав часы, минуты и секунды.
        ● Возможность создать TimeUnit, задав часы и минуты. Секунды тогда
        должны проставиться нулевыми
        ● Возможность создать TimeUnit, задав часы. Минуты и секунды тогда
        должны проставиться нулевыми.
        Публичные методы:
        ● Вывести на экран установленное в классе время в формате hh:mm:ss
        ● Вывести на экран установленное в классе время в 12-часовом формате
        (используя hh:mm:ss am/pm)
        ● Метод, который прибавляет переданное время к установленному в
        TimeUnit (на вход передаются только часы, минуты и секунды).
 */


public class TimeUnit {
    private int h;//Часы
    private int m;//Минуты
    private int s;//Секунды

    public TimeUnit(int hours, int minutes, int seconds) {
        if (TimeUnit.Validation.validationHours(hours)) {
            this.h = hours;
        } else throw new IllegalArgumentException("Не корректное значение . Максимальное значение 23");
        if (TimeUnit.Validation.validationMinutes(minutes)) {
            this.m = minutes;
        } else throw new IllegalArgumentException("Не корректное значение . Максимальное значение 60");
        if (TimeUnit.Validation.validationSeconds(seconds)) {
            this.s = seconds;
        } else throw new IllegalArgumentException("Не корректное значение . Максимальное значение 60");
    }

    public class Validation {
        public static boolean validationHours(int hours){
            return hours>=0 && hours<=23;
        }
        public static boolean validationMinutes(int minutes){
            return minutes>=0 && minutes<=59;
        }
        public static boolean validationSeconds(int seconds){
            return seconds>=0 && seconds<=59;
        }
    }
    public TimeUnit(int hours) {
        if (TimeUnit.Validation.validationHours(hours)) {
            this.h = hours;
        } else throw new IllegalArgumentException("Не корректное значение . Максимальное значение 23");
    }
//    public TimeUnit(int hours, int minutes, int seconds) {
//        //Конструктор чч мм сc
//        if (hours < 0 || hours > 23) {
//            System.out.println("Error time format");
//            this.h = 0;
//        } else
//            this.h = hours;//Если даже ошибка
//        if (minutes < 0 || minutes > 59) {
//            System.out.println("Error time format");
//            this.m = 0;
//        } else
//            this.m = minutes;//Если даже ошибка
//        if (seconds < 0 || seconds > 59) {
//            System.out.println("Error time format");
//            this.s = 0;
//        } else
//            this.s = seconds;//Если даже ошибка
//    }

    public TimeUnit(int hh, int minutes) {
        //Конструктор чч мм сc
        if (hh < 0 || hh > 23) {
            System.out.println("Error time format");
            this.h = 0;
        } else
            this.h = hh;//Если даже ошибка
        if (minutes < 0 || minutes > 59) {
            System.out.println("Error time format");
            this.m = 0;
        } else
            this.m = minutes;//Если даже ошибка
        this.s = 0;
    }

//    public TimeUnit(int hours) {
//        //Конструктор чч мм сc
//        if (hours < 0 || hours > 23) {
//            System.out.println("Error time format");
//            this.h = 0;
//        } else
//            this.h = hours;//Если даже ошибка
//        this.m = 0;
//        this.s = 0;
//    }

    public void formatOutAsia() {
        //Вывод в обычном
        System.out.printf("%d%d:%d%d:%d%d\n", h / 10, h % 10, m / 10, m % 10, s / 10, s % 10);
    }

    public void formatOutUsa() {
        //Вывод в виде AM PM
        if (h > 12 || h == 0) {
            h = h % 12;
            System.out.printf("%d%d:%d%d:%d%d PM\n", h / 10, h % 10, m / 10, m % 10, s / 10, s % 10);
        } else {
            System.out.printf("%d%d:%d%d:%d%d AM\n", h / 10, h % 10, m / 10, m % 10, s / 10, s % 10);
        }
    }

    public void addTime(int hours, int minutes, int seconds) {
        int sec = hours * 60 * 60 + minutes * 60 + seconds;
        int sec_cur = h * 60 * 60 + m * 60 + s;
        int ans = sec + sec_cur;
        h = (ans / 3600) % 24;
        ans %= 3600;
        m = (ans / 60) % 60;
        s = ans % 60;
    }

}


