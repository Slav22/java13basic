package Oop;
/*Необходимо реализовать класс DayOfWeek для хранения порядкового номера
дня недели (byte) и названия дня недели (String).
Затем в отдельном классе в методе main создать массив объектов DayOfWeek
длины 7. Заполнить его соответствующими значениями (от 1 Monday до 7
Sunday) и вывести значения массива объектов DayOfWeek на экран.
Пример вывода:
1 Monday
2 Tuesday
…
7 Sunday

 */


public class DayOfWeek {
    private byte dayNum;//Порядковый номер дня недели
    private  String name;//Название
    public DayOfWeek(byte a, String b)
    {
        this.dayNum=a;
        this.name=b;
    }
    //Геттеры

    public String getName() {
        return name;
    }

    public byte getDayNum() {
        return dayNum;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDayNum(byte dayNum) {
        this.dayNum = dayNum;
    }
}

