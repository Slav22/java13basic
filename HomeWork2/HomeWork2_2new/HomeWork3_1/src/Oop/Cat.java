package Oop;
/*1. Необходимо реализовать класс Cat.
У класса должны быть реализованы следующие приватные методы:
● sleep() — выводит на экран “Sleep”
● meow() — выводит на экран “Meow”
● eat() — выводит на экран “Eat”
И публичный метод:
status() — вызывает один из приватных методов случайным образом.
 */
import java.util.Random;

public class Cat {
    public static void main(String[] args) {
        Cat misha = new Cat();  // для тестирования
        Cat cot = new Cat();
        misha.status();
        cot.status();
    }


    //Реализуем приватные методы
    private void sleep() {
        System.out.println("Sleep");
    }

    private void meow() {
        System.out.println("Meow");
    }

    private void eat() {
        System.out.println("Eat");
    }

    //публичный метод
    public void status() {
        Random random = new Random();//Рандом класс
        int rand = random.nextInt() % 3;
        switch (rand) {
            case 0: {
                sleep();
                break;
            }
            case 1: {
                meow();
                break;
            }
            case 2: {
                eat();
                break;
            }
        }
    }
}


