import java.util.Scanner;

/*Петя решил начать следить за своей фигурой. Но все существующие
приложения для подсчета калорий ему не понравились и он решил написать
свое. Петя хочет каждый день записывать сколько белков, жиров, углеводов и
калорий он съел, а в конце недели приложение должно его уведомлять,
вписался ли он в свою норму или нет.
На вход подаются числа A — недельная норма белков, B — недельная норма
жиров, C — недельная норма углеводов и K — недельная норма калорий.
Затем передаются 7 строк, в которых в том же порядке указаны сколько было
съедено Петей нутриентов в каждый день недели. Если за неделю в сумме по
каждому нутриенту не превышена недельная норма, то вывести “Отлично”,
иначе вывести “Нужно есть поменьше”.
Ограничения:
● 0 < A, B, C < 2000
● 0 < ai, bi, ci < 2000
● 0 < K < 20000
● 0 < ki < 20000

 */
public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        final int A = scanner.nextInt();
        final int B = scanner.nextInt();
        final int C = scanner.nextInt();
        final int K = scanner.nextInt();
 //       System.out.println(A + " " + B + " " + C + " " + K);
        int[][] a = new int[7][4];
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 4; j++) {
                a[i][j] = scanner.nextInt();
            }
        }
        int[] b = new int[4];
        for (int column = 0; column < a[0].length; column++) {
            int total = 0;
            for (int row = 0; row < a.length; row++)
                total += a[row][column];
//            if (A < total) {
//                System.out.println("Нужно есть поменьше");
//                break;
//
//        } else if (A > total) {
//            if (B < total) {
//                System.out.println("Нужно есть поменьше");
//                break;
//            } else if (B > total) {
//                if (C < total) {
//                    System.out.println("Нужно есть поменьше");
//                    break;
//                } else if (C > total) {
//                    if (K < total) {
//                        System.out.println("Нужно есть поменьше");
//                        break;
//                    } else if (K > total) {
//                        System.out.println("Отлично");
//
//
//                    }
//
//                }
//
//
//            }
//
//
//        }


//            System.out.println("Сумма для столбца " + column
//                    + " равна " + total);
            if (column == 0) {
                b[0] = total;
            } else if (column == 1) {
                b[1] = total;
            } else if (column == 2) {
                b[2] = total;
            } else if (column == 3) {
                b[3] = total;

                }

        }

        if (b[0] > A){
            System.out.println("Нужно есть поменьше");

        } else if (b[1] > B) {
            System.out.println("Нужно есть поменьше");
        } else if (b[2] > C) {
            System.out.println("Нужно есть поменьше");
        } else if (b[3] > K) {
            System.out.println("Нужно есть поменьше");
        }else
            System.out.println("Отлично");

 //       for (int i = 0; i < b.length; i++) {
//            System.out.print(b[i] + " ");
 //       }
 //           System.out.println("Отлично");


//            for (int i = 0; i < 7; i++) {
//                System.out.print(a[i][0]);
//                for (int j = 1; j < 4; j++) {
//                    //              System.out.print(a[0][j]);
//                    System.out.print(" " + a[i][j]);
//                }
//                System.out.println();
//            }

        }
    }

