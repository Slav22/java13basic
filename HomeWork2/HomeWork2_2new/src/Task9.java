import java.util.Scanner;

/**
 * На вход подается число N. Необходимо вывести цифры числа слева направо.
 * Решить задачу нужно через рекурсию.
 * Ограничения:
 * 0 < N < 1000000
 */
public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(recursion(n)); // вызов рекурсивной функции
    }

    public static String recursion(int n) {
        // Базовый случай
        if (n < 10) {
            return Integer.toString(n);
        } // Шаг рекурсии / рекурсивное условие
        else {
            return recursion(n / 10) + " " + n % 10;
        }
    }
}