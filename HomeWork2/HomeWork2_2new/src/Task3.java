/**На вход подается число N — количество строк и столбцов матрицы. Затем
 передаются координаты X и Y расположения коня на шахматной доске.
 Необходимо заполнить матрицу размера NxN нулями, местоположение коня
 отметить символом K, а позиции, которые он может бить, символом X.
 Ограничения:
 ● 4 < N < 100
 ● 0 <= X, Y < N
 */

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[][] a = new String[n][n];
        int x = scanner.nextInt();
        int y = scanner.nextInt();


        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = "0";
            }
        }
        for (int i = y; i <= y; i++) {

            for (int j = x; j <= x; j++) {

                if (i == y && j == x) {
                    a[i][j] = "K";
                    if (i - 1 >= 0 && j - 2 >= 0) {
                        a[i - 1][j - 2] = "X";
                    }
                    if (i - 2 >= 0 && j - 1 >= 0) {
                        a[i - 2][j - 1] = "X";
                    }
                    if (i + 1 < n && j + 2 < n) {
                        a[i + 1][j + 2] = "X";
                    }
                    if (i + 2 < n && j + 1 < n) {
                        a[i + 2][j + 1] = "X";
                    }
                    if (i - 1 > 0 && j + 2 < n) {
                        a[i - 1][j + 2] = "X";
                    }
                    if (i + 2 < n && j - 1 >= 0) {
                        a[i + 2][j - 1] = "X";
                    }
                    if (i + 1 < n && j - 2 >= 0) {
                        a[i + 1][j - 2] = "X";
                    }
                    if (i - 2 >= 0 && j + 1 < n) {
                        a[i - 2][j + 1] = "X";
                    }
                }
            }
        }


        for (int i = 0; i < n; i++) {
            System.out.print(a[i][0]);
            for (int j = 1; j < n; j++) {
                System.out.print(" " + a[i][j]);

            }
            System.out.println();
        }
    }
}





//    public static boolean isCorrectMove(String move) {
//        if (!move.matches("[A-H][1-9]-[A-H][1-9]")) {
//            return false;
//        }
//
//        int x = Math.abs(move.charAt(0) - move.charAt(3));
//        int y = Math.abs(move.charAt(1) - move.charAt(4));
//
//        return x + y == 3;
//    }
//}