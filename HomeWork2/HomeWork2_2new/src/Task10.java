import java.util.Scanner;

/**
 * На вход подается число N. Необходимо вывести цифры числа справа налево.
 * Решить задачу нужно через рекурсию.
 * Ограничения:
 * 0 < N < 1000000
 */
public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        System.out.println(recursion(n)); // вызов рекурсивной функции
    }

    public static int recursion(int n) {
        // Базовый случай
        if (n < 10) {
            return n;
        }// Шаг рекурсии / рекурсивное условие
        else {
            System.out.print(n % 10 + " ");
            return recursion(n / 10);
        }
    }
}