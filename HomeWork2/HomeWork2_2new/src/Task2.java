/**На вход подается число N — количество строк и столбцов матрицы. Затем в
 последующих двух строках подаются координаты X (номер столбца) и Y (номер
 строки) точек, которые задают прямоугольник.
 Необходимо отобразить прямоугольник с помощью символа 1 в матрице,
 заполненной нулями (см. пример) и вывести всю матрицу на экран.
 Ограничения:
 ● 0 < N < 100
 ● 0 <= X1
 , Y1
 , X2
 , Y2 < N
 ● X1 < X2
 ● Y1 < Y2
 */
import java.util.Scanner;

//import java.util.Arrays;
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        char v = '\u0001';
        int n = scanner.nextInt();
        int[][] a = new int[n][n];
        int x1 = scanner.nextInt();
        int y1 = scanner.nextInt();
        int x2 = scanner.nextInt();
        int y2 = scanner.nextInt();



//        int[][] a = new int[n][n];


        //заполняем наш массив

//        int[][] b = new int[x][y];
        for (int i = y1; i <= y2; i++) {
            for (int j = x1; j <= x2; j++) {
                if (i == y1 || i == y2 || j == x1 || j == x2) {
                    a[i][j] = 1;
                }
            }
        }




        for (int i = 0; i < n; i++) {
            System.out.print(a[i][0]);
            for (int j = 1; j < n; j++) {
                //              System.out.print(a[0][j]);
                System.out.print(" " + a[i][j]);
            }
            System.out.println();
        }

    }
}
