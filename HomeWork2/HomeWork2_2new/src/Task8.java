import java.util.Scanner;

/**
 * На вход подается число N. Необходимо посчитать и вывести на экран сумму его
 * цифр. Решить задачу нужно через рекурсию.
 * Ограничения:
 * 0 < N < 1000000
 */
public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(recursion(n)); // вызов рекурсивной функции
    }


        public static int recursion(int n) {
            // Базовый случай
            if (n < 10) {
                return n;
            }// Шаг рекурсии / рекурсивное условие
            else {
                return n % 10 + recursion(n / 10);
            }
        }
}