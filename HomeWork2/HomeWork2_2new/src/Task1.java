/**На вход передается N — количество столбцов в двумерном массиве и M —
        количество строк. Затем сам передается двумерный массив, состоящий из
        натуральных чисел.
        Необходимо сохранить в одномерном массиве и вывести на экран
        минимальный элемент каждой строки.
        Ограничения:
        ● 0 < N < 100
        ● 0 < M < 100
        ● 0 < ai < 1000
 */
//import java.util.Arrays;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();

        //заполняем наш массив
        int[][] a = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = scanner.nextInt();
            }
        }
        //вывод на экран нашего массива для наглядности

        int[] res = new int[n];
        //подсчет min
        int min;
        for (int i = 0; i < a.length; i++) {
            min = a[i][0];
            for (int j = 0; j < a[i].length; j++) {
                if (min >= a[i][j]) {
                    min = a[i][j];
                    res[i] = min;
                }
            }


        }
        //       System.out.println(Arrays.toString(res));
        for (int j : res) {
            System.out.print(j + " ");
        }
    }
}
























