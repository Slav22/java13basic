/*На вход подается число N — количество строк и столбцов матрицы.
Затем передается сама матрица, состоящая из натуральных чисел.
Необходимо вывести true, если она является симметричной относительно
побочной диагонали, false иначе.
Побочной диагональю называется диагональ, проходящая из верхнего правого
угла в левый нижний.
Ограничения:
● 0 < N < 100
● 0 < ai < 1000

 */
import java.util.Scanner;

//import java.util.Arrays;
public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[][] a = new int[n][n];


//        int[][] a = new int[n][n];


        //заполняем наш массив

//        int[][] b = new int[x][y];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = scanner.nextInt();
            }
        }
        boolean sim = true;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n-i-1; j++) {
                if (a[i][j] != a[n-j-1][n-i-1]) {
                    sim = false; // оказалась несимметричной
                }
            }
        System.out.println(sim);
    }
}





//        for (int i = 0; i < n; i++) {
//            System.out.print(a[i][0]);
//            for (int j = 1; j < n; j++) {
//                //              System.out.print(a[0][j]);
//                System.out.print(" " + a[i][j]);
//            }
//            System.out.println();
//        }
//
//    }
//}
