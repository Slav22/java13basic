import java.util.Locale;
import java.util.Scanner;
import java.util.Collections;
import java.util.Arrays;
/**Раз в год Петя проводит конкурс красоты для собак. К сожалению, система
 хранения участников и оценок неудобная, а победителя определить надо. В
 первой таблице в системе хранятся имена хозяев, во второй - клички животных,
 в третьей — оценки трех судей за выступление каждой собаки. Таблицы
 связаны между собой только по индексу. То есть хозяин i-ой собаки указан в i-ой
 строке первой таблицы, а ее оценки — в i-ой строке третьей таблицы. Нужно
 помочь Пете определить топ 3 победителей конкурса.
 На вход подается число N — количество участников конкурса. Затем в N
 строках переданы имена хозяев. После этого в N строках переданы клички
 собак. Затем передается матрица с N строк, 3 вещественных числа в каждой —
 оценки судей. Победителями являются три участника, набравшие
 максимальное среднее арифметическое по оценкам 3 судей. Необходимо
 вывести трех победителей в формате “Имя хозяина: кличка, средняя оценка”.
 Гарантируется, что среднее арифметическое для всех участников будет
 различным.
 Ограничения:
 0 < N < 10
 *
 */
public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Locale.setDefault(Locale.US);
        int n = scanner.nextInt();

        String[] a = new String[n];
        String[] b = new String[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.next();
        }
        for (int i = 0; i < n; i++) {
            b[i] = scanner.next();
        }

        float[][] c = new float[n][3];
        for (int row = 0; row < c.length; row++) {
            for (int column = 0; column < c[row].length; column++) {
                c[row][column] = scanner.nextFloat();
            }
        }
 //       System.out.println("Закончен ввод");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
        for (int i = 0; i < b.length; i++) {
            System.out.print(b[i] + " ");
        }
        System.out.println();
        for (int row = 0; row < c.length; row++) {
            for (int column = 0; column < c[row].length; column++) {
                System.out.print(c[row][column] + " ");
            }
            System.out.println();
        }
 //       System.out.println("Закончен вывод");
        float[] d = new float[n];
        for (int row = 0; row < c.length; row++) {
            float total = 0;
            for (int column = 0; column < c[0].length; column++)


                total += c[row][column] / 3;


            d[row] = total;
        }

            float max = d[0];
            int indexOfMax = 0;
            for (int i = 1; i < d.length; i++) {
                if (d[i] > max) {
                    max = d[i];
                    indexOfMax = i;

                }
            }
        System.out.print(a[indexOfMax] + ":" + b[indexOfMax] + ",");
        System.out.printf("%.1f",(float)(Math.round( max*100))/100);
         max = d[0];
        indexOfMax = 0;
        for (int i = 1; i < d.length; i++) {
            if (d[i] > max) {
                max = d[i];
                indexOfMax = i;

            }
        }
        System.out.print(a[indexOfMax] + ":" + b[indexOfMax] + ",");
        System.out.printf("%.1f",(float)(Math.round( max*100))/100);
//            System.out.printf(a[indexOfMax] + ":" + b[indexOfMax] + "," + ("%.1f",(double)(Math.round( max*100))/100));
        }

 //       Arrays.sort(d); // сортируем
//        for (int i = 0; i < d.length; i++) {
//            System.out.print(d[i] + " ");
//            System.out.println();
////            System.out.println(total);
//        }
    }




