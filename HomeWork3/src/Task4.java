import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
4. Написать метод, который с помощью рефлексии получит все интерфейсы
класса, включая интерфейсы от классов-родителей и интерфейсов-родителей
 */
public class Task4 {
    public static void main(String[] args) {
        List<Class<?>> result = getAllInterfaces(D.class);
        for (Class<?> cls : result) {
            System.out.println(cls.getName());
        }
    }

    public static List<Class<?>> getAllInterfaces(Class<?> cls) {
        List<Class<?>> interfaces = new ArrayList<>();
        while (cls != Object.class) {
            interfaces.addAll(Arrays.asList(cls.getInterfaces()));
            cls = cls.getSuperclass();
        }
        return interfaces;
    }
}




