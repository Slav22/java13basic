import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/*
Есть класс APrinter:
public class APrinter {
public void print(int a) {
System.out.println(a);
}
}
Задача: с помощью рефлексии вызвать метод print() и обработать все
возможные ошибки (в качестве аргумента передавать любое подходящее
число). При “ловле” исключений выводить на экран краткое описание ошибки.
 */
public class Task3 {
    public static void main(String[] args) {

        Class<APrinter> cls = APrinter.class;
        try {

            Method method = cls.getDeclaredMethod("print", int.class);
            String result = method.getName();
            try {
                method.invoke(new APrinter(45),50);
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
            System.out.println(result);
            System.out.println(method);
          //  System.out.println(obj);

        } catch (NoSuchMethodException e) {
            System.out.println("No such method: " + e.getMessage());
        } catch (IllegalArgumentException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
