/*
2. Написать метод, который рефлексивно проверит наличие аннотации @IsLike на
любом переданном классе и выведет значение, хранящееся в аннотации, на
экран.

 */


public class Task2 {
    public static void main(String[] args) {
        printClassDescription(Task1.class);
    }

    public static void printClassDescription(Class<?> cls) {
        if (!cls.isAnnotationPresent(IsLike.class)) {
            return;
        }
        IsLike classDescription = cls.getAnnotation(IsLike.class);
        System.out.println("Аннотация " + classDescription);


    }
}

