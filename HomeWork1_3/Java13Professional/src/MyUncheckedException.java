/*
2. Создать собственное исключение MyUncheckedException, являющееся
непроверяемым.
 */

import java.util.Scanner;

public class MyUncheckedException extends Exception {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();


        try {
            toDivide(20, n);
        } catch (MyUncheckedException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void toDivide(int a, int b) throws MyUncheckedException {
        try {
            System.out.println(a / b);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("Выход из программы.");
        }
    }
}
