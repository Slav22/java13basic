
import java.util.ArrayList;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Locale;



public class FormValidator {


    private String nameOfPerson;
    private String birthdateOfPerson;
    private String gender;
    private String height;


    public static void main(String[] args) throws Exception {
        ArrayList<FormValidator> list = new ArrayList<>();


        // тестирование - корректный ввод
        list.add(new FormValidator("Vyacheslav", "22.07.1962", "MALE", "182.3"));


        list.add(new FormValidator("Elena", "21.07.1963", "FEMALE", "160.5"));


        // тестирование - НЕ корректный ввод
        list.add(new FormValidator("Elena", "12.04.2022 ", "FEMALE", "150"));
        System.out.println(list);


    }

    public FormValidator(String nameOfPerson, String birthdateOfPerson, String gender, String height) throws Exception {
        this.nameOfPerson = nameOfPerson;
        checkName(nameOfPerson);
        this.birthdateOfPerson = birthdateOfPerson;
        checkBirthdate(birthdateOfPerson);
        this.gender = gender;
        checkGender(gender);
        this.height = height;
        checkHeight(height);
    }


    public void checkName(String str) throws Exception {
        if (str.length() < 2 || str.length() > 20) throw new Exception("Неверный ввод имени");
        if (!Character.isUpperCase(str.charAt(0))) {
            throw new Exception("Первая буква маленькая");
        }
    }


    public void checkBirthdate(String str) throws Exception {

        Date sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH).parse(str);

        Date date1;
        date1 = new Date("01/01/1900");

        Date date2;
        date2 = new Date("12/12/2022");

        if (date1.after(sdf) || date2.before(sdf)) throw new Exception("Ошибка! Не верна  дата рождения ");

    }


    public void checkGender(String str) throws Exception {

        if (!str.equals(String.valueOf(Gender.MALE)) && !str.equals(String.valueOf(Gender.FEMALE)))
            throw new Exception("Ошибка! Пол  ");


    }

    public void checkHeight(String str) throws Exception {
        if (Double.parseDouble(str) < 0) throw new Exception("Не корректный ввод роста");

    }

    @Override
    public String toString() {
        return "FormValidator{" +
                "nameOfPerson='" + nameOfPerson + '\'' +
                ", birthdateOfPerson='" + birthdateOfPerson + '\'' +
                ", gender='" + gender + '\'' +
                ", height='" + height + '\'' +
                '}';
    }

}
