import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        inputN();
        System.out.println("Успешный ввод!");
    }
    private static void inputN() {
        System.out.println("Введите число n, 0 < n < 100");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n < 0 || n > 100) {
            try {
                throw new Exception("Неверный ввод");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}

