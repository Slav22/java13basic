/*
4. Создать класс MyEvenNumber, который хранит четное число int n. Используя
исключения, запретить создание инстанса MyPrimeNumber с нечетным числом.
 */

import java.util.Scanner;

public class MyEvenNumber {

    public static void main(String[] args)  {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        if (n % 2 == 0) {
            System.out.println("Все правильно");

        } else
            try {
                throw new Exception("Нечетное");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
    }
}




