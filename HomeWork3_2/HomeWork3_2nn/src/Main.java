import java.util.*;

public class Main {

    //добавить книгу в библиотеку c выводом результата добавления
    public static void addBook(Library lib, Book b) {
        if (lib.addBook(b)) { //если успешно добавляется
            System.out.println("Книга " + b.getName() + " успешно добавлена");
        }
        else {
            System.out.println("Книга с названием " + b.getName() + " уже есть в библиотеке!");
        }
    }

    //удалить книгу из библиотеки с выводом результата
    public static void removeBook(Library lib, String bookName) {
        if (lib.removeBookByName(bookName))
            System.out.println("Книга " + bookName + " удалена");
        else
            System.out.println("Книга не удалена! Указанной книги " + bookName + " нет в библиотеке, либо она одолжена посетителем");
    }

    //одолжить посетителю книгу с выводом результат
    public static void ownBook(Library lib, String bookName, Client c) {
        if (lib.ownBook(bookName, c) ) { //если успешно выдали
            System.out.println("Книга " + bookName + " выдана клиенту " + c.getId() + "\n");
        }
        else {
            System.out.println("Книга " + bookName + " не одолжена посетителю " + c.getName() + ", так не выполнено одно из условий:");
            System.out.println("""
                    a. Она есть в библиотеке\r
                    b. У посетителя сейчас нет книги\r
                    c. Она не одолжена
                    """);
        }
    }

    //вернуть книгу, взятую клиентом c и поставить оценку rate
    public static void backBook(Library lib, Client c, int rate) {
        if (lib.clientBacksBook(c, rate)) {
            System.out.println("Посетитель " + c.getId() + " вернул книгу и поставил ей оценку " + rate);
        }
        else {
            System.out.println("Посетитель " + c.getId() + " не смог вернуть книгу");
        }
    }

    public static void main(String[] args) {

        Library library = new Library(); //создаем библиотеку пустую

        //книги для добавления в библиотеку
        Book book1 = new Book("Э. Успенский", "Чебурашка и крокодил Гена");
        Book book2 = new Book("Стивен Кинг", "Зеленая Миля");
        Book book3 = new Book("Стивен Кинг", "1408");

        //добавляем книги
        addBook(library, book1);
        addBook(library, book2);
        addBook(library, book3);
        //вывод библиотеки
        System.out.println("\n1) Все книги:");
        library.printAllBooks();

        addBook(library, new Book("Степан Королев", "Зеленая Миля")); // попытка добавить книгу с имеющимся названием

        //выборка книг автора
        ArrayList<Book> selected = library.selectBooksByAuthor("Стивен Кинг");
        System.out.println("\n2) Книги автора Стивен Кинг: ");
        for (Book b : selected) {
            System.out.println(b);
        }
        System.out.println();

        //посетители, которые будут брать книги
        Client c1 = new Client("Борисов А. Л.");
        Client c2 = new Client("Барсукова Т.Ф.");

        ownBook(library, "Одуванчики-цветочки", c1); //одалживаем несуществующую
        ownBook(library, book1.getName(), c1); //одалживаем доступную
        ownBook(library, book1.getName(), c1); //пробуем тому же посетителю выдать еще одну книгу
        ownBook(library, book1.getName(), c2); //другому посетителю ту же книгу
        ownBook(library, book2.getName(), c2); //другому посетителю дотсупную книгу

        //вывод данных о посетителях
        System.out.println("3) " + c1 + "\n");
        System.out.println(c2 + "\n");

        //вывод библиотеки
        System.out.println("4) Текущий список книг:");
        library.printAllBooks();

        //вывод доступных книг
        System.out.println("5) Текущий список доступных книг:");
        library.printAvailableBooks();

        //удаление книг
        System.out.println("6) ");
        removeBook(library, "Оно"); //удалить несуществующую
        removeBook(library, book1.getName()); //удалить выданную
        removeBook(library, book3.getName()); //удалить доступную
        System.out.println();

        //возврат и оценка книг
        backBook(library, c1, 4);
        backBook(library, c2, 5);

        //вывод библиотеки
        System.out.println("\n7) Текущий список книг:");
        library.printAllBooks();

        //несколько раз берем-возвращаем книги для проверки накопления оценок
        ownBook(library, book1.getName(), c2);
        ownBook(library, book2.getName(), c1);
        backBook(library, c1, 3);
        backBook(library, c2, 5);
        ownBook(library, book2.getName(), c1);
        ownBook(library, book1.getName(), c2);
        backBook(library, c1, 4);
        backBook(library, c2, 2);
        ownBook(library, book2.getName(), c2);
        ownBook(library, book1.getName(), c1);
        backBook(library, c1, 3);
        backBook(library, c2, 5);

        System.out.println("\n8) Текущий список книг:");
        library.printAllBooks();
    }

}
