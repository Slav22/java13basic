//класс для описания посетителя библиотеки
public class Client {
    private String name; //имя
    private String id; //идентификатор
    private Book bookOwned; //книга на руках

    Client(String name) {
        this.name = name;
        this.id = null; //идентификатор при создании пуст
        this.bookOwned = null; // книга не взята
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public Book getBookOwned() {
        return bookOwned;
    }
    public void setBookOwned(Book bookOwned) {
        this.bookOwned = bookOwned;
    }

    //информация о клиенте в виде строки
    @Override
    public String toString() {
        String info = this.name;
        if (id != null) { //если идентификатор был получен
            info += "; id: " + this.id;
        }
        if (bookOwned != null) { //если есть книга на руках
            info += "\nОдолжена книга: " + this.bookOwned;
        }
        return info;
    }



}
