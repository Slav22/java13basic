import java.util.ArrayList;


public class Library {

    ArrayList<Book> books; //список книг

    //в конструкторе инициализируем пустой список книг
    public Library() {
        this.books = new ArrayList<>();
    }

    //добавить книгу
    //true, если книги не было и она добавлена
    //false, если книга с таким названием уже была
    public boolean addBook(Book book) {
        if (this.searchBookByName(book.getName()) == null) { //если книги с таким названием нет в списке
            books.add(book); //добавляем в список
            return true; //успешно добавили
        }
        else { //иначе добавление не происходит
            return false;
        }
    }

    //поиск книги по названию
    public Book searchBookByName(String name) {
        for (Book b : books) { //просмотр книг списка
            if (b.getName().equals(name)) { //если нашли книгу с заданными именем
                return b;
            }
        }
        return null; //если не нашли
    }

    //вывод всего списка книг
    public void printAllBooks() {
        for (Book b : books) {
            System.out.println(b + "\n");
        }
    }

    //вывод доступных книг
    public void printAvailableBooks() {
        for (Book b : books) {
            if (b.getOwnedBy() == null) { //если книга не одолжена никем
                System.out.println(b + "\n");
            }
        }
    }

    //удалить книгу по названию
    public boolean removeBookByName(String name) {
        Book b = this.searchBookByName(name); //поиск указанной книги
        if (b != null && b.getOwnedBy() == null) { //если книга есть в списке и она не одолжена
            books.remove(b); //удаляем книгу
            return true;
        }
        else {
            return false; //иначе удаление не происходит
        }
    }

    //выборка книг по автору с получением списка
    public ArrayList<Book> selectBooksByAuthor(String author) {
        ArrayList<Book> result = new ArrayList<>();
        for (Book b : books) {
            if (b.getAuthor().equals(author)) {
                result.add(b);
            }
        }
        return result;
    }

    //посетитель одалживает книгу
    public boolean ownBook(String bookName, Client c) {
        if (c.getBookOwned() == null) { //если у клиента нет одолженной книги
            Book b = this.searchBookByName(bookName); //выполняем поиск книги
            if (b != null && b.getOwnedBy() == null) { //если книга есть и она не одолжена
                //связываем объекты посетителя и книги
                b.setOwnedBy(c);
                c.setBookOwned(b);
                if (c.getId() == null) { //если посетитель пришел впервые
                    //присваиваем идентификатор имя + длина имени + длина названия книги
                    String id = "id" + c.getName() + c.getName().length() + b.getName().length();
                    c.setId(id);
                }
                return true;
            }
        }
        return false;
    }

    //посетитель пришел возвращать книгу и дать свою оценку
    public boolean clientBacksBook(Client client, int rating) {
        if (client != null && client.getBookOwned() != null) {
            client.getBookOwned().addRating(rating); //добавляем оценку к книге
            client.getBookOwned().setOwnedBy(null); //книга никем не одолжена
            client.setBookOwned(null); //у клиента больше нет книги
            return true;
        }
        return false;
    }
}
