//класс Книга
public class Book {
    private String name; //название
    private String author; //автор
    private Client ownedBy; //кем одолжена книга
    private int ratingSum; //сумма оценок
    private int ratingCount; //количество оценок

    public Book(String author, String name) {
        this.name = name;
        this.author = author;
        this.ownedBy = null; //книга никем не одолжена
        this.ratingSum = 0; //оценки еще не выставлялись
        this.ratingCount = 0;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }

    public Client getOwnedBy() {
        return ownedBy;
    }
    public void setOwnedBy(Client ownedBy) {
        this.ownedBy = ownedBy;
    }

    public int getRatingSum() {
        return ratingSum;
    }
    public void setRatingSum(int ratingSum) {
        this.ratingSum = ratingSum;
    }

    public int getRatingCount() {
        return ratingCount;
    }
    public void setRatingCount(int ratingCount) {
        this.ratingCount = ratingCount;
    }

    //добавить новую оценку
    public void addRating(int rate) {
        this.ratingSum += rate; //учитываем в сумме оценок
        this.ratingCount += 1; //+1 к количеству
    }

    //вычисление рейтинга как среднее арифметическое оценок
    public double calcRating() {
        if (this.ratingCount != 0) { //если оценки выставлялись
            return this.ratingSum / (double)this.ratingCount; //среднее арифмтетическое
        }
        else {
            return 0.0;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(author).append(" - ").append(name);
        sb.append(". Рейтинг: ").append(this.calcRating());
        if (this.ownedBy != null) { //если книга одолжена
            sb.append("\nНа руках у посетителя: ").append(ownedBy.getId());
        }
        return sb.toString();
    }

}
