import java.util.Scanner;

public class Students {


    Scanner input = new Scanner(System.in);

    Student[] theStudents = new Student[50];


    public static int count = 0;

    public void addStudent(Student s) {

        for (int i = 0; i < count; i++) {

            if (s.regNum.equalsIgnoreCase(theStudents[i].regNum)) {

                System.out.println("Читатель с ID " + s.regNum + " уже зарегистрирован.");
                return;
            }

        }

        if (count <= 50) {

            theStudents[count] = s;
            count++;

        }

    }

    public void showAllStudents() {

        System.out.println("Имя \t ID читателя");
        for (int i = 0; i < count; i++) {

            System.out.println(theStudents[i].studentName + "\t\t" + theStudents[i].regNum);

        }


    }

    public int isStudent() {


        System.out.println("Введите ID читателя:");
        String regNum = input.nextLine();

        for (int i = 0; i < count; i++) {

            if (theStudents[i].regNum.equalsIgnoreCase(regNum)) {

                return i;

            }

        }
        System.out.println("Читатель не зарегистрирован.");
        System.out.println("Зарегистрируйте с начала.");


        return -1;

    }

    public void checkOutBook(Books book) {
        int studentIndex = this.isStudent();

        if (studentIndex != -1) {
            System.out.println("Читатель проверен");

            book.showAllBooks();
            Book b = book.checkOutBook();
 //           System.out.println("книга добавлена");
            if (b != null) {

                if (theStudents[studentIndex].booksCount < 1) {
                    System.out.println("добавление книги");
                    theStudents[studentIndex].borrowedBooks[theStudents[studentIndex].booksCount] = b;
                    theStudents[studentIndex].booksCount++;

                } else {

                    System.out.println("Читатель не может иметь более 1 книги.");

                }
                return;
            }
            System.out.println("Книга доступна.");

        }

    }

    public void checkInBook(Books book) {

        int studentIndex = this.isStudent();
        if (studentIndex != -1) {
            System.out.println("ID\t\t\tНазвание\t\t\tАвтор");
            Student s = theStudents[studentIndex];
            for (int i = 0; i < s.booksCount; i++) {

                System.out.println(s.borrowedBooks[i].sNo + "\t\t\t" + s.borrowedBooks[i].bookName + "\t\t\t" +
                        s.borrowedBooks[i].authorName);

            }
            System.out.println("Введите ID выданной книги:");
            int sNo = input.nextInt();
            for (int i = 0; i < s.booksCount; i++) {

                if (sNo == s.borrowedBooks[i].sNo) {

                    book.checkInBook(s.borrowedBooks[i]);
                    s.borrowedBooks[i] = null;
                    System.out.println("Просим Вас оценить рейтинг книги от 0 до 10, где 0 - плохо, 10 - великолепно ");
                    int rating = input.nextInt();

                    return;

                }


            }
            System.out.println("Книги № " + sNo + "не найдено");


        }
    }
}



