import java.util.Scanner;

public class Books {


    Book[] theBooks = new Book[50];     // Array that stores 'book' Objects.


    public static int count;    // Counter for No of book objects Added in Array.

    Scanner input = new Scanner(System.in);
//    private  theStudents;


    public int compareBookObjects(Book b1, Book b2) {
        if (b1.bookName.equalsIgnoreCase(b2.bookName)) {
            System.out.println("Книга с таким названием уже есть в библиотеке.");
            return 0;
        }

        if (b1.sNo == b2.sNo) {
            System.out.println("Книга с таким ID уже есть.");
            return 0;
        }
        return 1;
    }

    public void addBook(Book b) {
        for (int i = 0; i < count; i++) {
            if (this.compareBookObjects(b, this.theBooks[i]) == 0)
                return;
        }
        if (count < 50) {
            theBooks[count] = b;
            count++;
        } else {
            System.out.println("В библиотеке нет больше места для книг.");
        }
    }

    public void searchBySno() {

        System.out.println("\t\t\t\tПОИСК ПО ID\n");
        int sNo;
        System.out.println("Введите ID книги:");
        sNo = input.nextInt();


        int flag = 0;
        for (int i = 0; i < count; i++) {

            if (sNo == theBooks[i].sNo) {
//                outputs();
                System.out.println("ID\t\tНазвание\t\tАвтор\t\tЖанр\t\tрейтинг\t\tДоступное количество\t\tВсего количество");
                System.out.println(theBooks[i].sNo + "\t\t" + theBooks[i].bookName + "\t\t" + theBooks[i].authorName +
                        "\t\t" + theBooks[i].genreName + "\t\t" + theBooks[i].rating + "\t\t" + theBooks[i].bookQtyCopy + "\t\t" + theBooks[i].bookQty);
                flag++;
//               return;
            }
        }
        if (flag == 0)
            System.out.println(" Книги с ID " + sNo + " нет.");
    }

    public void searchByGenreName() {
        System.out.println("\t\t\t\t ПООИСК ПО ЖАНРУ ");
        //       input.nextLine();
        System.out.println("Введите жанр:");
        String genreName = input.nextLine();
        int flag = 0;


        for (int i = 0; i < count; i++) {
            if (!genreName.equalsIgnoreCase(theBooks[i].genreName)) {
                continue;
            }
            //               outputs();
            System.out.println("ID\t\tНазвание\t\tАвтор\t\tЖанр\t\tрейтинг\t\tДоступное количество\t\tВсего количество");
            System.out.println(theBooks[i].sNo + "\t\t" + theBooks[i].bookName + "\t\t" + theBooks[i].authorName +
                    "\t\t" + theBooks[i].genreName + "\t\t" + theBooks[i].rating + "\t\t" + theBooks[i].bookQtyCopy + "\t\t" + theBooks[i].bookQty);
            flag++;
        }
        if (flag == 0)
            System.out.println("Книги - " + genreName + " не найдено.");

    }

    public void searchByRatingName() {
        System.out.println("\t\t\t\t ПОИСК ПО РЕЙТИНГУ КНИГИ ПО МНЕНИЮ ЧИТАТЕЛЕЙ");
        System.out.println("Ведите требуемый рейтинг книги от 0 до 10, где 0 - плохо, 10 - великолепно ");
        int rating = input.nextInt();

        int flag = 0;


        for (int i = 0; i < count; i++) {
            if (rating <= (theBooks[i].rating)) {
//                outputs();
                System.out.println("ID\t\tНазвание\t\tАвтор\t\tЖанр\t\tрейтинг\t\tДоступное количество\t\tВсего количество");
                System.out.println(theBooks[i].sNo + "\t\t" + theBooks[i].bookName + "\t\t" + theBooks[i].authorName +
                        "\t\t" + theBooks[i].genreName + "\t\t" + theBooks[i].rating + "\t\t" + theBooks[i].bookQtyCopy + "\t\t" + theBooks[i].bookQty);
                flag++;
            }
        }
        if (flag == 0)
            System.out.println("Книги c рейтингом более - " + rating + " не найдено.");

    }


    public void searchByAuthorName() {
        System.out.println("\t\t\t\t ПОИСК ПО НАЗВАНИЮ ИЛИ ФАМИЛИИ АВТОРА");
        System.out.println("Ведите название книги ");
        String bookName = input.nextLine();
        System.out.println("Ведите фамилию автора книги:");
        String authorName = input.nextLine();
        int flag = 0;

        //             outputs(flag);
        for (int i = 0; i < count; i++) {
            if (authorName.equalsIgnoreCase(theBooks[i].authorName) || (bookName.equalsIgnoreCase(theBooks[i].bookName))) {
                //               outputs();
                System.out.println("ID\t\tНазвание\t\tАвтор\t\tЖанр\t\tрейтинг\t\tДоступное количество\t\tВсего количество");
                System.out.println(theBooks[i].sNo + "\t\t" + theBooks[i].bookName + "\t\t" + theBooks[i].authorName +
                        "\t\t" + theBooks[i].genreName + "\t\t" + theBooks[i].rating + "\t\t" + theBooks[i].bookQtyCopy + "\t\t" + theBooks[i].bookQty);
                flag++;
            }

        }
        if (flag == 0)
            System.out.println("Книги - " + authorName + " не найдено.");

    }

    public void showAllBooks() {
        System.out.println("\t\t\t\tПОКАЗАТЬ ВСЕ КНИГИ \n");
        outputs();

    }


    public void deleteBook() {
        System.out.println("\t\t\t\tСПИСАНИЕ КНИГ \n");
        System.out.println("Ввести серийный номер книги ");
        int sNo = input.nextInt();
        for (int i = 0; i < count; i++) {
            if (sNo == theBooks[i].sNo) {
                System.out.println("Введите количество книг для списания:");
                int addingQty = input.nextInt();
                theBooks[i].bookQty -= addingQty;
                theBooks[i].bookQtyCopy = theBooks[i].bookQtyCopy - addingQty;
                return;
            }
        }
    }


    public void upgradeBookQty() {
        System.out.println("\t\t\t\tОБНОВЛЕНИЕ КОЛИЧЕСТВА КНИГ \n");
        System.out.println("Ввести ID книги ");
        int sNo = input.nextInt();
        for (int i = 0; i < count; i++) {
            if (sNo == theBooks[i].sNo) {
                System.out.println("Введите количество книг для пополнения:");
                int addingQty = input.nextInt();
                theBooks[i].bookQty += addingQty;
                theBooks[i].bookQtyCopy += addingQty;
                return;
            }
        }
    }

    public void dispMenu() {
        System.out.println("--------------------------------------------------------------------------");
        System.out.println("Введите 0 для выхода из приложения.");
        System.out.println("Введите 1 для добавления новой книги.");
        System.out.println("Введите 2 для уточнения количества книг.");
        System.out.println("Введите 3 для поиска книги.");
        System.out.println("Введите 4 для просмотра всей библиотеки.");
        System.out.println("Введите 5 для регистрации читателя.");
        System.out.println("Введите 6 для просмотра всех читателей.");
        System.out.println("Введите 7 для выдачи книги. ");
        System.out.println("Введите 8 для возврата книги");
        System.out.println("Введите 9 для списания книги");
        System.out.println("---------------------------------------------");

    }

    public int isAvailable(int sNo) {
        for (int i = 0; i < count; i++) {
            if (sNo == theBooks[i].sNo) {
                if (theBooks[i].bookQtyCopy > 0) {
                    System.out.println("Книга доступна");
                    return i;
                }
                System.out.println("Книга недоступна ");
                return -1;
            }
        }

        System.out.println("Нет книг с ID " + sNo);
        return -1;
    }

    public Book checkOutBook() {

        System.out.println("Введите ID книги, которая будет выдана.");
        int sNo = input.nextInt();
        int bookIndex = isAvailable(sNo);
        if (bookIndex != -1) {
            //int bookIndex = isAvailable(sNo);
            theBooks[bookIndex].bookQtyCopy--;
            return theBooks[bookIndex];
        }
        return null;
    }

    public void checkInBook(Book b) {
        for (int i = 0; i < count; i++) {
            if (b.equals(theBooks[i])) {
                theBooks[i].bookQtyCopy++;
                return;
            }
        }
    }

    public void outputs() {
        System.out.println("ID\tНазвание\tАвтор\t\tЖанр\tрейтинг\tДоступное количество\tВсего количество");
        for (int i = 0; i < count; i++) {

            System.out.println(theBooks[i].sNo + "\t\t" + theBooks[i].bookName + "\t\t" + theBooks[i].authorName +
                    "\t\t" + theBooks[i].genreName + "\t\t" + theBooks[i].rating + "\t\t" + theBooks[i].bookQtyCopy +
                    "\t\t" + theBooks[i].bookQty);
        }

    }

}