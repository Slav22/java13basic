package com.sber.java13spring.java13springproject.filmlibraryproject.service;

import com.sber.java13spring.java13springproject.filmlibraryproject.dto.DirectorDTO;
import com.sber.java13spring.java13springproject.filmlibraryproject.dto.DirectorWithFilmDTO;
import com.sber.java13spring.java13springproject.filmlibraryproject.mapper.DirectorMapper;
import com.sber.java13spring.java13springproject.filmlibraryproject.mapper.DirectorWithFilmMapper;
import com.sber.java13spring.java13springproject.filmlibraryproject.model.Director;
import com.sber.java13spring.java13springproject.filmlibraryproject.repository.DirectorRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DirectorService
        extends GenericService<Director, DirectorDTO> {
    //  Инжектим конкретный репозиторий для работы с таблицей books
    private final DirectorRepository repository;
    private final DirectorWithFilmMapper directorWithFilmMapper;

    protected DirectorService(DirectorRepository repository,
                          DirectorMapper mapper,
                          DirectorWithFilmMapper directorWithFilmMapper) {
        //Передаем этот репозиторй в абстрактный севрис,
        //чтобы он понимал с какой таблицей будут выполняться CRUD операции
        super(repository, mapper);
        this.repository = repository;
        this.directorWithFilmMapper = directorWithFilmMapper;
    }

    public List<DirectorWithFilmDTO> getAllDirectorWithFilms() {
        return directorWithFilmMapper.toDTOs(repository.findAll());
    }


}

