package com.sber.java13spring.java13springproject.filmlibraryproject.service;
import com.sber.java13spring.java13springproject.filmlibraryproject.dto.OrderDTO;
import com.sber.java13spring.java13springproject.filmlibraryproject.mapper.OrderMapper;
import com.sber.java13spring.java13springproject.filmlibraryproject.model.Order;
import com.sber.java13spring.java13springproject.filmlibraryproject.repository.OrderRepository;
import org.springframework.stereotype.Service;

@Service
public class OrderService
        extends GenericService<Order, OrderDTO> {

    protected OrderService (OrderRepository orderRepository,
                                  OrderMapper orderMapper) {
        super(orderRepository, orderMapper);
    }
}

