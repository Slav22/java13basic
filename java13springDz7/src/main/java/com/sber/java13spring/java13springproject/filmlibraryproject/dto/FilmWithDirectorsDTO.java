package com.sber.java13spring.java13springproject.filmlibraryproject.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class FilmWithDirectorsDTO extends FilmDTO {
    private Set<DirectorDTO> directors;
}

