package com.sber.java13spring.java13springproject.filmlibraryproject.dto;

import com.sber.java13spring.java13springproject.filmlibraryproject.model.Genre;
import lombok.*;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor

public class FilmDTO extends GenericDTO {

    private String publishYear;
    private String filmTitle;
    private String storagePlace;
    private Integer amount;
    private String country;

    private Genre genre;
    private Set<Long>directorIds;

}
