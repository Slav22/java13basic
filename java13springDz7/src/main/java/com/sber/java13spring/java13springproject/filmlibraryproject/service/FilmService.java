package com.sber.java13spring.java13springproject.filmlibraryproject.service;

import com.sber.java13spring.java13springproject.filmlibraryproject.dto.FilmDTO;
import com.sber.java13spring.java13springproject.filmlibraryproject.dto.FilmSearchDTO;
import com.sber.java13spring.java13springproject.filmlibraryproject.dto.FilmWithDirectorsDTO;
import com.sber.java13spring.java13springproject.filmlibraryproject.mapper.FilmMapper;
import com.sber.java13spring.java13springproject.filmlibraryproject.mapper.FilmWithDirectorMapper;
import com.sber.java13spring.java13springproject.filmlibraryproject.model.Film;
import com.sber.java13spring.java13springproject.filmlibraryproject.repository.FilmRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmService
        extends GenericService<Film, FilmDTO> {
    //  Инжектим конкретный репозиторий для работы с таблицей books
    private final FilmRepository repository;
    private final FilmWithDirectorMapper filmWithDirectorMapper;

    protected FilmService(FilmRepository repository,
                          FilmMapper mapper,
                          FilmWithDirectorMapper filmWithDirectorMapper) {
        //Передаем этот репозиторй в абстрактный севрис,
        //чтобы он понимал с какой таблицей будут выполняться CRUD операции
        super(repository, mapper);
        this.repository = repository;
        this.filmWithDirectorMapper = filmWithDirectorMapper;
    }

    public Page<FilmWithDirectorsDTO> getAllFilmWithDirectors(Pageable pageable) {
        Page<Film> filmsPaginated = repository.findAll(pageable);
        List<FilmWithDirectorsDTO> result = filmWithDirectorMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }

    public FilmWithDirectorsDTO getFilmWithDirectors(Long id) {
        return filmWithDirectorMapper.toDTO(mapper.toEntity(super.getOne(id)));
    }

    public Page<FilmWithDirectorsDTO> findFilms(FilmSearchDTO filmSearchDTO,
                                                Pageable pageable) {
        String genre = filmSearchDTO.getGenre() != null ? String.valueOf(filmSearchDTO.getGenre().ordinal()) : null;
        Page<Film> filmsPaginated = repository.searchBooks(genre,
                filmSearchDTO.getFilmTitle(),
                filmSearchDTO.getDirectorFio(),
                pageable
        );
        List<FilmWithDirectorsDTO> result = filmWithDirectorMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }
}




