package com.sber.java13spring.java13springproject.filmlibraryproject.repository;

import com.sber.java13spring.java13springproject.filmlibraryproject.model.Director;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepository
        extends GenericRepository<Director> {

}
