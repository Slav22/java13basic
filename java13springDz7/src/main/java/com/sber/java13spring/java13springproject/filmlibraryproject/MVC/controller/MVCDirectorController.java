package com.sber.java13spring.java13springproject.filmlibraryproject.MVC.controller;

import com.sber.java13spring.java13springproject.filmlibraryproject.dto.DirectorDTO;
import com.sber.java13spring.java13springproject.filmlibraryproject.dto.DirectorWithFilmDTO;
import com.sber.java13spring.java13springproject.filmlibraryproject.dto.FilmWithDirectorsDTO;
import com.sber.java13spring.java13springproject.filmlibraryproject.service.DirectorService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("directors")
public class MVCDirectorController {
    private final DirectorService directorService;

    public MVCDirectorController(DirectorService directorService) {
        this.directorService = directorService;
    }

    @GetMapping("")
    public String getAll(Model model) {
        List<DirectorWithFilmDTO> result = directorService.getAllDirectorWithFilms();
        model.addAttribute("directors", result);

        return "directors/viewAllDirectors";
    }

    @GetMapping("/add")
    public String create() {
        return "directors/addDirectors";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("directorForm") DirectorDTO directorDTO) {
        directorService.create(directorDTO);
        return "redirect:/directors";
    }
}