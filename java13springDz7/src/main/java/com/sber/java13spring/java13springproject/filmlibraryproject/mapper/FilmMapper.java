package com.sber.java13spring.java13springproject.filmlibraryproject.mapper;

import com.sber.java13spring.java13springproject.filmlibraryproject.dto.FilmDTO;
import com.sber.java13spring.java13springproject.filmlibraryproject.model.Film;
import com.sber.java13spring.java13springproject.filmlibraryproject.model.GenericModel;
import com.sber.java13spring.java13springproject.filmlibraryproject.repository.DirectorRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FilmMapper
        extends GenericMapper<Film, FilmDTO> {
    private final DirectorRepository directorRepository;

    protected FilmMapper(ModelMapper mapper, DirectorRepository directorRepository) {
        super(mapper, Film.class, FilmDTO.class);
        this.directorRepository = directorRepository;
    }

    @PostConstruct
    @Override
    public void setupMapper() {
        modelMapper.createTypeMap(Film.class, FilmDTO.class)
                .addMappings(m -> m.skip(FilmDTO::setDirectorIds)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(FilmDTO.class, Film.class)
                .addMappings(m -> m.skip(Film::setDirectors)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(Film::setPublishYear)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(FilmDTO source, Film destination) {
        if (!Objects.isNull(source.getDirectorIds())) {
            destination.setDirectors(new HashSet<>(directorRepository.findAllById(source.getDirectorIds())));
        }
        else {
            destination.setDirectors(Collections.emptySet());
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(source.getPublishYear(), formatter);
        destination.setPublishYear(date);
    }

    @Override
    protected void mapSpecificFields(Film source, FilmDTO destination) {
        destination.setDirectorIds(getIds(source));
    }

    protected Set<Long> getIds(Film films) {
        return Objects.isNull(films) || Objects.isNull(films.getDirectors())
                ? null
                : films.getDirectors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}

