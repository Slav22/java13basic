package com.sber.java13spring.java13springproject.filmlibraryproject.REST.controller;

import com.sber.java13spring.java13springproject.filmlibraryproject.dto.OrderDTO;
import com.sber.java13spring.java13springproject.filmlibraryproject.model.Order;
import com.sber.java13spring.java13springproject.filmlibraryproject.service.OrderService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rent/info")
@Tag(name = "Аренда фильмов",
        description = "Контроллер для работы с арендой/выдачей фильмов пользователям фильмотеки")
public class OrderController
        extends GenericController<Order, OrderDTO> {
    public OrderController(OrderService orderService) {
        super(orderService);
    }
}

