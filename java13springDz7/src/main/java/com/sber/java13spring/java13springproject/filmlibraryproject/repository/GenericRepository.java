package com.sber.java13spring.java13springproject.filmlibraryproject.repository;

import com.sber.java13spring.java13springproject.filmlibraryproject.model.GenericModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface GenericRepository <T extends GenericModel>
        extends JpaRepository <T, Long> {
}
