package com.sber.java13spring.java13springproject.filmlibraryproject.repository;

import com.sber.java13spring.java13springproject.filmlibraryproject.model.Film;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRepository
        extends GenericRepository<Film> {

    @Query(nativeQuery = true,
            value = """
                 select b.*
                 from films b
                 left join film_director ba on b.id = ba.film_id
                 join directors a on a.id = ba.director_id
                 where b.title ilike '%' || :title || '%'
                 and cast(b.genre as char) like coalesce(:genre,'%')
                 and a.fio ilike '%' || :fio || '%'
                      """)
    Page<Film> searchBooks(@Param(value = "genre") String genre,
                           @Param(value = "title") String title,
                           @Param(value = "fio") String fio,
                           Pageable pageable);
}

