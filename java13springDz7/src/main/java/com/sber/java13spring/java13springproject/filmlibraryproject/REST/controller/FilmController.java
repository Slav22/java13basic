package com.sber.java13spring.java13springproject.filmlibraryproject.REST.controller;

import com.sber.java13spring.java13springproject.filmlibraryproject.REST.controller.GenericController;
import com.sber.java13spring.java13springproject.filmlibraryproject.dto.FilmDTO;
import com.sber.java13spring.java13springproject.filmlibraryproject.model.Film;
import com.sber.java13spring.java13springproject.filmlibraryproject.service.FilmService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/films")
@Tag(name = "Фильмы",
        description = "Контроллер для работы с фильмами фильмотеки")
//localhost:9090/api/rest/books
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")

public class FilmController
        extends GenericController<Film, FilmDTO> {

    public FilmController(FilmService filmService) {
        super(filmService);
    }

//    @Operation(description = "Добавить автора к книге", method = "addAuthor")
//    @RequestMapping(value = "/addAuthor", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Book> addAuthor(@RequestParam(value = "bookId") Long bookId,
//                                          @RequestParam(value = "authorId") Long authorId) {
//        Book book = bookRepository.findById(bookId).orElseThrow(() -> new NotFoundException("Книга с переданным ID не найдена"));
//        Author author = authorRepository.findById(authorId).orElseThrow(() -> new NotFoundException("Автор с таким ID не найден"));
//        book.getAuthors().add(author);
//        return ResponseEntity.status(HttpStatus.OK).body(bookRepository.save(book));
//    }
}

