package com.sber.java13spring.java13springproject.filmlibraryproject.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class DirectorDTO
        extends GenericDTO {
    private String fio;
    private String position;
    private LocalDate birthDay;
    private String description;
    private Set<Long> filmsIds;

}
