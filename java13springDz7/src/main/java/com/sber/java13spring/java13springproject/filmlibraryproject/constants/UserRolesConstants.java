package com.sber.java13spring.java13springproject.filmlibraryproject.constants;
public interface UserRolesConstants {
    String ADMIN = "ADMIN";
    String LIBRARIAN = "LIBRARIAN";
    String USER = "USER";
}

