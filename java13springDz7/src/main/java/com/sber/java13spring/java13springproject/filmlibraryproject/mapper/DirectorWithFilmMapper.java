package com.sber.java13spring.java13springproject.filmlibraryproject.mapper;

import com.sber.java13spring.java13springproject.filmlibraryproject.dto.DirectorWithFilmDTO;
import com.sber.java13spring.java13springproject.filmlibraryproject.model.Director;
import com.sber.java13spring.java13springproject.filmlibraryproject.model.GenericModel;
import com.sber.java13spring.java13springproject.filmlibraryproject.repository.FilmRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DirectorWithFilmMapper
        extends GenericMapper<Director, DirectorWithFilmDTO> {


    private final FilmRepository filmRepository;

    protected DirectorWithFilmMapper(ModelMapper mapper,
                                     FilmRepository filmRepository
    ) {
        super(mapper, Director.class, DirectorWithFilmDTO.class);
        this.filmRepository = filmRepository;
    }


    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Director.class, DirectorWithFilmDTO.class)
                .addMappings(m -> m.skip(DirectorWithFilmDTO::setFilmsIds)).setPostConverter(toDtoConverter());

        modelMapper.createTypeMap(DirectorWithFilmDTO.class, Director.class)
                .addMappings(m -> m.skip(Director::setFilms)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(DirectorWithFilmDTO source, Director destination) {
        destination.setFilms(new HashSet<>(filmRepository.findAllById(source.getFilmsIds())));
    }

    @Override
    protected void mapSpecificFields(Director source, DirectorWithFilmDTO destination) {
        destination.setFilmsIds(getIds(source));
    }

    @Override
    protected Set<Long> getIds(Director entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getId())
                ? null
                : entity.getFilms().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}





