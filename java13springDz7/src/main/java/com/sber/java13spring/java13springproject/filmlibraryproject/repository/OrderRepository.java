package com.sber.java13spring.java13springproject.filmlibraryproject.repository;

import com.sber.java13spring.java13springproject.filmlibraryproject.model.Order;

public interface OrderRepository
        extends GenericRepository<Order> {
}
