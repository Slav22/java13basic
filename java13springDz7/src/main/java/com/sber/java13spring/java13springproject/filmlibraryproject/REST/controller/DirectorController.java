package com.sber.java13spring.java13springproject.filmlibraryproject.REST.controller;

import com.sber.java13spring.java13springproject.filmlibraryproject.dto.DirectorDTO;
import com.sber.java13spring.java13springproject.filmlibraryproject.model.Director;
import com.sber.java13spring.java13springproject.filmlibraryproject.service.DirectorService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/directors")
@Tag(name = "Режиссеры",
        description = "Контроллер для работы с режиссерами фильмов библиотеки")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DirectorController
        extends GenericController<Director, DirectorDTO> {

    private DirectorService directorService;


    public DirectorController(DirectorService directorService) {
        super(directorService);
        this.directorService = directorService;
    }

//    @Secured(value = "ROLE_ADMIN")
//    public void test() {
//
//    }

//    @Operation(description = "Добавить книгу к автору", method = "addBook")
//    @RequestMapping(value = "/addBook", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Author> addAuthor(@RequestParam(value = "bookId") Long bookId,
//                                            @RequestParam(value = "authorId") Long authorId) {
//        Book book = bookRepository.findById(bookId).orElseThrow(() -> new NotFoundException("Книга с переданным ID не найдена"));
//        Author author = genericRepository.findById(authorId).orElseThrow(() -> new NotFoundException("Автор с таким ID не найден"));
//        author.getBooks().add(book);
//        return ResponseEntity.status(HttpStatus.OK).body(genericRepository.save(author));
//    }
}


