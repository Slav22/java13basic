public class Participant {

    protected    String nameOfParticipant;
    public   double averageGrade;


    @Override
    public String toString() {
        return
                "Участник " + nameOfParticipant + '\'' +
                "ср.оценка " + averageGrade +
                '}';
    }

    public Participant(String nameOfParticipant, double averageGrade) {


        this.nameOfParticipant = nameOfParticipant;
        this.averageGrade = averageGrade;
    }



    public double getGrade() {
        return averageGrade;
    }

}
