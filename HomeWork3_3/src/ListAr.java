/*
3. На вход передается N — количество столбцов в двумерном массиве и M —
количество строк. Необходимо вывести матрицу на экран, каждый элемент
которого состоит из суммы индекса столбца и строки этого же элемента. Решить
необходимо используя ArrayList.
Ограничения:
● 0 < N < 100
● 0 < M < 100
Пример:
Входные данные Выходные данные
2 2

0 1
1 2

3 5

0 1 2
1 2 3
2 3 4
3 4 5
4 5 6
 */

import java.util.ArrayList;
import java.util.*;
import java.util.Arrays;



public class ListAr {


    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();

        //       Integer[][] matrix = new Integer[n][m];

        Integer[][] matrix = new Integer[m][n];
        for (int i = 0; i < matrix.length; i++) {
            matrix[i][0] = i;

            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = i + j;
            }

        }
        System.out.println("Вывод двумерного массива");
        System.out.println(Arrays.deepToString(matrix));

//  то же, через ArrayList
        List<ArrayList<Integer>> list = new ArrayList<>(m);

        for (int i = 0; i < m; i++) {

            ArrayList<Integer> list1 = new ArrayList<>(n);

            for (int j = 0; j <n; j++) {

                list1.add(i+j);

            }
            list.add(list1);
        }
        System.out.println("Список");
        System.out.println(list);
        System.out.println("в цикле");
        for (ArrayList<Integer> integers : list) {
            System.out.println(integers);

        }
    }

}





