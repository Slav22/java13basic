import java.util.ArrayList;
public class Main {
    public static void main(String[] args) {

        //тестирование 1 задачи

        ArrayList<Animal> zoo = new ArrayList<>();
        zoo.add(new Mammal("Flipper", "живородящий", "fish", "dolphin", "swimming", "fast"));
        zoo.add(new Dolphin("Flipper", "живородящий", "fish", "swimming", "fast"));
        zoo.add(new GoldenFish("Guppy", "мечет икру", "планктон", "swimming", "slow"));
        zoo.add(new Egle("Todd", "откладывает яйца", "мыши", "flying", "fast"));
        zoo.add(new Bat("Bat", "живородящий", "насекомые", "flying", "slow"));
        zoo.add(new Mammal("Tippi", "живородящий", "мясо", "cheetah", "running", "fast"));

        for (Animal a : zoo) {
            System.out.println(a);
        }
    }

}