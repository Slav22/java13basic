/*1. Рассматриваются следующие животные:
● летучая мышь (Bat)
● дельфин (Dolphin)
● золотая рыбка (GoldFish)
● орел (Eagle)
Все животные одинаково едят и спят (предположим), и никто из животных не
должен иметь возможности делать это иначе.
Еще животные умеют по-разному рождаться (wayOfBirth):
● Млекопитающие (Mammal) живородящие.
● Рыбы (Fish) мечут икру.
● Птицы (Bird) откладывают яйца.
Помимо этого бывают некоторые особенности, касающиеся передвижения.
Бывают летающие животные (Flying) и плавающие (Swimming). Однако орел
летает быстро, а летучая мышь медленно. Дельфин плавает быстро, а золотая
рыбка медленно.
Согласно этим утверждениям нужно создать иерархию, состоящую из классов,
абстрактных классов и/или интерфейсов. Каждое действие или утверждение
подразумевает под собой вызов void метода, в котором реализован вывод на
экран описания текущего действия.

 */



abstract class Animal {
   private final String name;
   private final String wayOfBirth;
   private final String favouriteFood;
    private final String animalName;

   private final String tMethod;
   private final String speedOfTrans;




    protected Animal(String name, String wayOfBirth, String favouriteFood, String animalName, String tMethod1,String speedOfTrans) {
//    protected Animal(String name, String animalName) {
        this.name = name;
        this.wayOfBirth = wayOfBirth;
        this.favouriteFood = favouriteFood;
        this.animalName = animalName;
        this.tMethod = tMethod1;
        this.speedOfTrans = speedOfTrans;

    }

    @Override
    public String toString() {
//        return animalName + " " + name  + " "+animalName+" преимущественно";
        return animalName + " " + name +" "+ wayOfBirth+" любит есть " + favouriteFood + " "+animalName+
                " преимущественно "+ tMethod+ " "+speedOfTrans ;
    }

}