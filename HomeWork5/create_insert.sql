/*Придумать как хранить в postgres следующую архитектуру:
Ирина, подруга Пети, решила создать свой бизнес по продаже цветов. Начать
она решила с самых основ: создать соответствующую базу данных для своего
бизнеса. Она точно знает, что будет продавать Розы по 100 золотых монет за
единицу, Лилии по 50 и Ромашки по 25.
Помимо этого, ей хочется хранить данные своих покупателей (естественно они
дали согласие на хранение персональной информации). Сохранять нужно Имя
и Номер телефона.
И, конечно, данные самого заказа тоже нужно как-то хранить! Ирина пока не
продумала поля, но она точно хочет следовать следующим правилам:
● в рамках одного заказа будет продавать только один вид цветов
(например, только розы)
● в рамках одного заказа можно купить от 1 до 1000 единиц цветов.
А также она представляет, какие запросы хочет делать к базе:
1. По идентификатору заказа получить данные заказа и данные клиента,
создавшего этот заказ
2. Получить данные всех заказов одного клиента по идентификатору
клиента за последний месяц
3. Найти заказ с максимальным количеством купленных цветов, вывести их
название и количество
4. Вывести общую выручку (сумму золотых монет по всем заказам) за все
время
Выполненное домашнее задание должно содержать:
1. Sql-скрипты создания таблиц с необходимыми связями
2. Sql-скрипты наполнения таблиц (тестово добавить несколько строк данных для
последующей проверки select’ов)
3. Sql-скрипты получения данных в соответствии с условие

 */
--откываем таблицу цветов
create table flowers
(
    id         serial primary key,
    title      varchar(30) NOT NULL,
    price     integer not null,
    date_added timestamp   not null
);
select *
from flowers;
--наполняем таблицу цветов
insert into  flowers(title, price, date_added)
values ('Роза аргентинская', '100', now());
insert into  flowers(title, price, date_added)
values ('Лилия', '50', now());
insert into  flowers(title, price, date_added)
values ('Ромашка', '25', now());

--откываем таблицу покупателей
create table customer
(
    id           serial primary key,
    customer_name         varchar(30) not null,
    phone_number varchar(12) not null,
    date_added   timestamp   not null
);
select *
from customer;
--наполняем таблицу покупателей
insert into customer(customer_name, phone_number, date_added)
values ('Дарья',+79513333333,now());
insert into customer(customer_name, phone_number, date_added)
values ('Ольга',+79514444444,now());
insert into customer(customer_name, phone_number, date_added)
values ('Мария',+79515555555,now());
insert into customer(customer_name, phone_number, date_added)
values ('Сергей',+79516666666,now());
insert into customer(customer_name, phone_number, date_added)
values ('Прохор',+79517777777,now());
insert into customer(customer_name, phone_number, date_added)
values ('Николай',+79518888888,now());
insert into customer(customer_name, phone_number, date_added)
values ('Петр',+79518888888,now());



--откываем таблицу заказов, связанную с покупателями
create table order_flowers
(
    id         serial primary key,
    customer_id integer REFERENCES customer (id),
    flowers_id integer REFERENCES flowers (id),
    quantity     integer       not null,
    date_added timestamp   not null,
    check ( quantity <=1000 )
);

select *
from order_flowers;

--наполняем таблицу заказов
insert into order_flowers(customer_id,flowers_id,quantity,date_added)
values (1,2,550,now());
insert into order_flowers(customer_id,flowers_id,quantity,date_added)
values (5,3,50,now());
insert into order_flowers(customer_id,flowers_id,quantity,date_added)
values (8,1,50,now());
insert into order_flowers(customer_id,flowers_id,quantity,date_added)
values (4,3,50,now());
insert into order_flowers(customer_id,flowers_id,quantity,date_added)
values (2,2,550,now());
insert into order_flowers(customer_id,flowers_id,quantity,date_added)
values (3,1,100,now());

update order_flowers
    set date_added = '2022-12-22 12:12:12.12345'
where id = 14;
commit ;

drop table flowers;
drop table order_flowers;
drop table customer;

