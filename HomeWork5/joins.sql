/*
 1. По идентификатору заказа получить данные заказа и данные клиента,
создавшего этот заказ
 */
select q.id as Номер_заказа,n.customer_name as Имя,n.phone_number as Телефон,q.date_added as Дата,q.quantity as Количество
from order_flowers q join   customer n on q.customer_id = n.id where q.id =3;

/*
 2. Получить данные всех заказов одного клиента по идентификатору
клиента за последний месяц
 */
select *  from order_flowers right join  customer n on n.id = order_flowers.customer_id where customer_name = 'Иван'  and order_flowers.date_added  between '2022-12-26 00:00:00' and now() ;
/*
 4. Вывести общую выручку (сумму золотых монет по всем заказам) за все
время
 */
select sum(quantity * f.price)
from order_flowers join flowers f on f.id = order_flowers.flowers_id;
/*
 Факультативно тренировался
 */
select  q.quantity as Количество from order_flowers  q join customer c on q.id = c.id;
select count(*)
from order_flowers;
select  max(quantity)
from order_flowers;
select  min(quantity)
from order_flowers;
select avg(quantity)
from order_flowers;
SELECT title,price FROM flowers WHERE flowers.title LIKE 'Роза%';
select * from customer as b, order_flowers as r where b.id = r.customer_id;
select *
from customer c left join order_flowers r on c.id = r.customer_id;
select n.customer_name as Имя, sum(q.quantity) as Количество
from customer n join order_flowers q on n.id = q.customer_id group by n.customer_name;
SELECT n.customer_name Имя,f.title,sum(q.quantity * f.price) as Баланс
FROM customer n  left join order_flowers q   ON n.id = q.customer_id
            left join flowers f  ON q.id = f.id group by   customer_name, f.title;
SELECT n.customer_name Имя,f.title Цветок,f.price Цена,sum(q.quantity) Куплено, sum(q.quantity * f.price) as Баланс
FROM customer n  join order_flowers q   ON n.id = q.customer_id
                 cross  join flowers f   group by   customer_name, f.title, f.price order by customer_name;
SELECT q.id as Номер_заказа,n.customer_name Имя,f.title Цветок,f.price Цена,sum(q.quantity) Куплено, sum(q.quantity * f.price) as Баланс
FROM customer n  join order_flowers q   ON n.id = q.customer_id
                  cross join flowers f   group by   customer_name, f.title, f.price, q.id order by customer_name ;
SELECT q.id as Номер_заказа,n.customer_name as Имя ,f.title Цветок,f.price Цена,sum(q.quantity) Куплено,sum(q.quantity * f.price) as Баланс
FROM customer n  join order_flowers q   ON n.id = q.customer_id
                  join flowers f on q.flowers_id = f.id  group by   customer_name, f.title, f.price, q.id order by customer_name;