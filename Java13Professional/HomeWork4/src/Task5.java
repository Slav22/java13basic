import java.util.List;
import java.util.stream.Collectors;

/*
5. На вход подается список непустых строк. Необходимо привести все символы строк к
верхнему регистру и вывести их, разделяя запятой.
Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ.
 */
public class Task5 {
    public static void main(String[] args) {
        List<String> stringList = List.of("abc", "def", "qqq");
        System.out.print(stringList.stream()
                .map(String::toUpperCase)
                .collect(Collectors.joining(",")));
    }
}
