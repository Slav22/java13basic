import java.util.Collection;
import java.util.Set;
import static java.lang.System.out;

/*
6. Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>.

 */
public class Task6 {

    public static void main(String[] args) {
        Set<Set<Integer>> sets = Set.of(Set.of(0, 1, 2), Set.of(3, 4, 5), Set.of(6, 7, 8), Set.of(9, 10, 11));

                sets.stream()
                .flatMap(Collection::stream)
                .map((s) -> s + " ")
                .forEach((s) -> out.print(s));
    }
}
