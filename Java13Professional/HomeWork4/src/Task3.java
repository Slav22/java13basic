import java.util.List;
/*
3. На вход подается список строк. Необходимо вывести количество непустых строк в
списке.
Например, для List.of("abc", "", "", "def", "qqq") результат равен 3.
 */
public class Task3 {
    public static void main(String[] args) {
        List<String> stringList = List.of("abc", "", "", "def", "qqq");

        long count = stringList.stream()
                .filter(name -> !name.isEmpty())
                .count();
        System.out.println(count);
    }
}
