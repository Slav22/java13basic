import java.util.ArrayList;
/*
1. Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на
экран.
 */
public class Task1 {
    public static void main(String[] args) {
        ArrayList<Integer> array = new ArrayList<>();
        for (int i = 0; i <= 100; i++) {
            array.add(i);
        }
        // System.out.println(array);
        int count = array.stream()
                .filter(i -> i % 2 == 0)
                .mapToInt(dbl -> dbl).sum();
        System.out.println(count);

    }
}
