import java.util.List;

/*
2. На вход подается список целых чисел. Необходимо вывести результат перемножения
этих чисел.
Например, если на вход передали List.of(1, 2, 3, 4, 5), то результатом должно быть число
120 (т.к. 1 * 2 * 3 * 4 * 5 = 120).

 */
public class Task2 {
    public static void main(String[] args) {
        List<Integer> num = List.of(1, 2, 3, 4, 5);

        int s = num.stream()
                .mapToInt(a -> a)
                .reduce(1, (a, b) -> a * b);
        System.out.println(s);
    }
}
