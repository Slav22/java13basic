import java.util.Comparator;
import java.util.List;

/*
4. На вход подается список вещественных чисел. Необходимо отсортировать их по
убыванию.

 */
public class Task4 {
    public static void main(String[] args) {

        List<Double> list = List.of(2.5,3.5,2.8,1.6,0.5);

        list.stream()
                .sorted(Comparator.reverseOrder())
                .forEach(System.out::println);
    }
}