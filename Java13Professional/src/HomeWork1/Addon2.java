package HomeWork1;/*
2. На вход подается число n, массив целых чисел отсортированных по
возрастанию длины n и число p. Необходимо найти индекс элемента массива
равного p. Все числа в массиве уникальны. Если искомый элемент не найден,
вывести -1.
Решить задачу за логарифмическую сложность.
Пример:
Входные данные Выходные данные
5
-42 -12 3 5 8
5
                 3


2               -1
17 19
20
 */


import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Addon2 {
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();            // N длинна отсортированного массива

        Integer[] ai = new Integer[N];
        for (int i = 0; i < ai.length; i++) { // заполняем массив элементами
            ai[i] = scanner.nextInt();
        }
        Arrays.sort(ai, Comparator.naturalOrder()); // сортируем
        int p = scanner.nextInt();
        System.out.println(binarySearch(ai,p));

    }
        static int binarySearch(Integer[] ai, int p) {
            // в начале левая и правая границы равны первому и последнему элементу массива
            var left = 0;
            var right = ai.length - 1;
            // пока левая и правая границы поиска не пересеклись
            while (left <= right) {
                // индекс текущего элемента находится посередине
                var middle = (left + right) / 2;
                var current = ai[middle];

                if (current == p) {
                    // нашли элемент - возвращаем его индекс
                    return middle;
                } else if (current < p) {
                    // текущий элемент меньше искомого - сдвигаем левую границу
                    left = middle + 1;
                } else {
                    // иначе сдвигаем правую границу
                    right = middle - 1;
                }
            }
            // проверили весь массив, но не нашли элемент
            return -1;
        }

    }

