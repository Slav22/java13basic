package HomeWork1;/*1. На вход подается число n и массив целых чисел длины n.
        Вывести два максимальных числа в этой последовательности.
        Пояснение: Вторым максимальным числом считается тот, который окажется
        максимальным после вычеркивания первого максимума.
        Пример:
        Входные данные Выходные данные
        5
        1 3 5 4 5
        5 5
        3
        3 2 1
        3 2


 */


import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;




public class Addon1 {
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();            // N длинна отсортированного массива

        Integer[] ai = new Integer[N];                // инициализируем отсортированный массив длинной N

        for (int i = 0; i < ai.length; i++) { // заполняем массив элементами
            ai[i] = scanner.nextInt();
        }
        Arrays.sort(ai, Comparator.reverseOrder()); // сортируем
        System.out.println(ai[0]+ " "+ ai[1]);


        }

    }

