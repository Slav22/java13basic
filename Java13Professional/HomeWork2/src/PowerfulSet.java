/*
3. Реализовать класс PowerfulSet, в котором должны быть следующие методы:
a. public <T> Set<T> intersection(Set<T> set1, Set<T> set2) — возвращает
пересечение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
Вернуть {1, 2}
b. public <T> Set<T> union(Set<T> set1, Set<T> set2) — возвращает
объединение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
Вернуть {0, 1, 2, 3, 4}
c. public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) —
возвращает элементы первого набора без тех, которые находятся также
и во втором наборе. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {3}
 */

import java.util.HashSet;
import java.util.Set;

public class PowerfulSet {
    public static void main(String[] args) {


        Set<Integer> set1 = new HashSet<>();


        set1.add(1);
        set1.add(2);
        set1.add(3);

        Set<Integer> set2 = new HashSet<>();
        set2.add(0);
        set2.add(1);
        set2.add(2);
        set2.add(4);

      //  set1.retainAll(set2);// пересечение двух наборов
      //  set1.addAll(set2);//объединение двух наборов
        set1.removeAll(set2);//возвращает элементы первого набора без тех, которые находятся также и во втором наборе.
        for (Integer element : set1) {
            System.out.print(element + " ");
        }
//        set1.forEach(elem -> System.out.println(elem)); // варианты вывода
//        set1.forEach(System.out::println);

        //Collections.disjoint()
    }
}



