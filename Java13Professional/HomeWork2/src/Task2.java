/*
2. С консоли на вход подается две строки s и t. Необходимо вывести true, если
одна строка является валидной анаграммой другой строки и false иначе.
Анаграмма — это слово или фраза, образованная путем перестановки букв
другого слова или фразы, обычно с использованием всех исходных букв ровно
один раз.
 */
import java.util.HashMap;
import java.util.Map;
public class Task2 {

    // Функция для проверки, являются ли `X` и `Y` анаграммами или нет
    public static boolean isAnagram(String X, String Y)
    {
        // базовый вариант
        if (X == null || Y == null) {
            return false;
        }

        // если длина X не совпадает с длиной Y, они не могут быть анаграммой
        if (X.length() != Y.length()) {
            return false;
        }

        // создаем пустую map
        Map<Character, Integer> freq = new HashMap<>();

        // вести подсчет каждого символа `X` на map
        for (char c: X.toCharArray()) {
            freq.put(c, freq.getOrDefault(c, 0) + 1);
        }

        // делаем для каждого символа `y` из `Y`
        for (char c: Y.toCharArray())
        {
            // если `y` не найден на map, т.е. либо `y` не присутствует
            // в строке `X` или имеет больше вхождений в строке `Y`
            if (!freq.containsKey(c)) {
                return false;
            }

            // уменьшаем частоту `y` на map
            freq.put(c, freq.get(c) - 1);

            // если его частота становится равной 0, стираем его с map
            if (freq.get(c) == 0) {
                freq.remove(c);
            }
        }

        // возвращаем true, если map становится пустой
        return freq.isEmpty();
    }
}
