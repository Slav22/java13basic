import java.util.*;


/*
Реализовать метод, который принимает массив words и целое положительное число k.
Необходимо вернуть k наиболее часто встречающихся слов..
Результирующий массив должен быть отсортирован по убыванию частоты
встречаемого слова. В случае одинакового количества частоты для слов, то
отсортировать и выводить их по убыванию в лексикографическом порядке.
Пример:
Входные данные Выходные данные
words =
["the","day","is","sunny","the","the","the",
"sunny","is","is","day"]
k = 4
["the","is","sunny","day"]
 */
public class Addon {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("the");
        list.add("day");
        list.add("day");
        list.add("day");
        list.add("day");
        list.add("day");
        list.add("day");
        list.add("is");
        list.add("is");
        list.add("sunny");
        list.add("sunny");
        list.add("sunny");
        list.add("sunny");
        list.add("the");
        list.add("the");
        list.add("the");
        list.add("sunny");
        list.add("sunny");
        list.add("is");
        list.add("is");
        list.add("day");
        list.add("in");
        list.add("in");
        list.add("in");
        list.add("in");
        list.add("in");
        int k = 4;

        NavigableMap<String, Integer> dMap = new TreeMap<>(getWordCount(list));
        dMap.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .limit(k)
                .forEach(System.out::println);

    }

    public static Map<String, Integer> getWordCount(List<String> words) {


        NavigableMap<String, Integer> result = new TreeMap<>();
        for (String value : words) {
            int counter = 1;
            if (result.containsKey(value)) {
                counter = result.get(value);
                counter++;
            }
            result.put(value, counter);
        }
        return result.descendingMap();
    }

}








