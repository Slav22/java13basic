import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/*
1. Реализовать метод, который на вход принимает ArrayList<T>, а возвращает
набор уникальных элементов этого массива. Решить используя коллекции.
 */
public class Task1 {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(7);
        list.add(3);
        list.add(4);
        list.add(2);
        list.add(1);
        list.add(7);
        list.add(7);

        System.out.println(listToSet(list));
    }

    public static<T> Set<T> listToSet(ArrayList<T> list) {
        Set<T> set = new HashSet<>(list);
        set.addAll(list);
        return set;

    }
}


