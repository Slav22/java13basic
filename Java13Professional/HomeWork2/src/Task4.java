/*
4. В некоторой организации хранятся документы (см. класс Document). Сейчас все
документы лежат в ArrayList, из-за чего поиск по id документа выполняется
неэффективно. Для оптимизации поиска по id, необходимо помочь сотрудникам
перевести хранение документов из ArrayList в HashMap.
        public class Document {
            public int id;
            public String name;
            public int pageCount;
        }
        Реализовать метод со следующей сигнатурой:
        public Map<Integer, Document> organizeDocuments(List<Document> documents)
                */

import java.util.ArrayList;
import java.util.HashMap;

import java.util.List;
import java.util.Map;



public class Task4 {
    public static void main(String[] args) {
        List<Document> list = new ArrayList<>();
        list.add(new Document(0, "Трудовой договор Пономарева В.Г.", 6));
        list.add(new Document(1, "Доп. соглашение Пономарева В.Г", 3));
        list.add(new Document(2, "Трудовой договор Иванова И.И.", 6));
        list.add(new Document(3, "Доп. соглашение Иванова И.И.", 3));
        list.add(new Document("Трудовой договор Петров И.И.", 7));


        System.out.println(organizeDocuments(list));
    }


    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> map = new HashMap<>();
        for (Document element : documents)
            map.put(element.id, element);
        return map;
    }
}
