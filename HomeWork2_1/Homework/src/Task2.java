import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // int a, b;
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        // double c = 0;
        double c = Math.sqrt((Math.pow(a, 2) + Math.pow(b, 2))/2);
        System.out.println(c);
    }
}
