import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        final long SECONDS_PER_MINUTE = 60;
        final long MINUTES_PER_HOURS = 60;
        long count;
        count = scanner.nextLong();
        long currentMinutes;
        currentMinutes = count / MINUTES_PER_HOURS % SECONDS_PER_MINUTE;
        System.out.println(count/MINUTES_PER_HOURS/SECONDS_PER_MINUTE + " " + currentMinutes);

    }
}
