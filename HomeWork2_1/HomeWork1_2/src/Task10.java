import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in);
        double n = scanner.nextDouble();
        System.out.println(Math.log(Math.exp(n)) == n);
    }
}
