import java.util.Scanner;

public class Project {
    public static void main(String[] args) {
        final double RUBLES_PER_YAN = 9.421;

        int yans;
        double rubles;
        int digit;

        Scanner input = new Scanner(System.in);

        // Получить сумму денег в китайских Юанях

        System.out.print("Введите сумму денег в китайских юанях: ");
        yans = input.nextInt();

        // отобразить сумму денег в китайских юанях
        //  с правильным окончанием
        System.out.print(yans);

        if(5 <= yans && yans <= 20)
            System.out.print(" китайских юаней равны");
        else {
            digit = yans % 10;

            if(digit == 1)
                System.out.print(" китайский юань равен");
            else if(2 <= digit && digit <= 4)
                System.out.print(" китайских юаня равны");
            else
                System.out.print(" китайских юаней равны");
        }

        // Конвертировать сумму денег в российские рубли
        rubles = RUBLES_PER_YAN * yans;

        // Отобразить сумму денег в российских рублях в пользу покупателя

        System.out.println(" " + (int)(rubles * 100) / 100.0
                + " российского рубля.");
    }
}

