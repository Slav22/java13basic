package com.sber.java13spring.java13springproject.libraryproject.dto;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DirectorDTO
      extends GenericDTO {
    private String directorFio;
    private String birthDate;
    private String description;
    private Set<Long> filmsIds;
    private boolean isDeleted;
}
