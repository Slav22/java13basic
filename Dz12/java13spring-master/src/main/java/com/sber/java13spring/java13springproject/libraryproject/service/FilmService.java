package com.sber.java13spring.java13springproject.libraryproject.service;

import com.sber.java13spring.java13springproject.libraryproject.constants.Errors;
import com.sber.java13spring.java13springproject.libraryproject.dto.FilmDTO;
import com.sber.java13spring.java13springproject.libraryproject.dto.FilmSearchDTO;
import com.sber.java13spring.java13springproject.libraryproject.dto.FilmWithDirectorsDTO;
import com.sber.java13spring.java13springproject.libraryproject.exception.MyDeleteException;
import com.sber.java13spring.java13springproject.libraryproject.mapper.FilmMapper;
import com.sber.java13spring.java13springproject.libraryproject.mapper.FilmWithDirectorsMapper;
import com.sber.java13spring.java13springproject.libraryproject.model.Film;
import com.sber.java13spring.java13springproject.libraryproject.repository.FilmRepository;
import com.sber.java13spring.java13springproject.libraryproject.utils.FileHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class FilmService
      extends GenericService<Film, FilmDTO> {
    //  Инжект конкретный репозиторий для работы с таблицей films
    private final FilmRepository repository;
    private final FilmWithDirectorsMapper filmWithDirectorsMapper;
    
    protected FilmService(FilmRepository repository,
                          FilmMapper mapper,
                          FilmWithDirectorsMapper filmWithDirectorsMapper) {
        //Передаем этот репозиторий в абстрактный сервис,
        //чтобы он понимал с какой таблицей будут выполняться CRUD операции
        super(repository, mapper);
        this.repository = repository;
        this.filmWithDirectorsMapper = filmWithDirectorsMapper;
    }
    
    public Page<FilmWithDirectorsDTO> getAllFilmsWithDirectors(Pageable pageable) {
        Page<Film> filmsPaginated = repository.findAll(pageable);
        List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }

    public Page<FilmWithDirectorsDTO> getAllNotDeletedFilmsWithDirectors(Pageable pageable) {
        Page<Film> filmsPaginated = repository.findAllByIsDeletedFalse(pageable);
        List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }
    
    public FilmWithDirectorsDTO getFilmWithDirectors(Long id) {
        return filmWithDirectorsMapper.toDTO(mapper.toEntity(super.getOne(id)));
    }
    
    public Page<FilmWithDirectorsDTO> findFilms(FilmSearchDTO filmSearchDTO,
                                                Pageable pageable) {
        String genre = filmSearchDTO.getGenre() != null ? String.valueOf(filmSearchDTO.getGenre().ordinal()) : null;
        Page<Film> filmsPaginated = repository.searchFilms(genre,
                                                           filmSearchDTO.getFilmTitle(),
                                                           filmSearchDTO.getDirectorFio(),
                                                           pageable
                                                          );
        List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }
    

    public FilmDTO create(final FilmDTO object,
                          MultipartFile file) {
        String fileName = FileHelper.createFile(file);
        object.setOnlineCopyPath(fileName);
        object.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        object.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }
    
    public FilmDTO update(final FilmDTO object,
                          MultipartFile file) {
        String fileName = FileHelper.createFile(file);
        object.setOnlineCopyPath(fileName);
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }
    
    @Override
    public void deleteSoft(Long id) throws MyDeleteException {
        Film film = repository.findById(id).orElseThrow(
              () -> new NotFoundException("Фильма с заданным ID=" + id + " не существует"));

        boolean filmCanBeDeleted = repository.checkFilmForDeletion(id);
        if (filmCanBeDeleted) {
            if (film.getOnlineCopyPath() != null && !film.getOnlineCopyPath().isEmpty()) {
                FileHelper.deleteFile(film.getOnlineCopyPath());
            }
            markAsDeleted(film);
            repository.save(film);
        }
        else {
            throw new MyDeleteException(Errors.Films.FILM_DELETE_ERROR);
        }
    }
    
    public void restore(Long objectId) {
        Film film = repository.findById(objectId).orElseThrow(
              () -> new NotFoundException("Фильма с заданным ID=" + objectId + " не существует"));
        unMarkAsDeleted(film);
        repository.save(film);
    }
}
