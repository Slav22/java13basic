package com.sber.java13spring.java13springproject.libraryproject.REST.controller;

import com.sber.java13spring.java13springproject.libraryproject.dto.FilmDTO;
import com.sber.java13spring.java13springproject.libraryproject.model.Film;
import com.sber.java13spring.java13springproject.libraryproject.service.FilmService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/films")
@Tag(name = "Фильмы",
        description = "Контроллер для работы с фильмами фильмотеки")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
//localhost:9090/api/rest/films
public class FilmController
        extends GenericController<Film, FilmDTO> {

    public FilmController(FilmService filmService) {
        super(filmService);
    }

}
