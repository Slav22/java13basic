package com.sber.java13spring.java13springproject.libraryproject.constants;

public interface FileDirectoriesConstants {
    String FILMS_UPLOAD_DIRECTORY = "files/films";
}
