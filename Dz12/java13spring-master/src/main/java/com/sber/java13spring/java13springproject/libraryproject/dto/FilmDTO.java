package com.sber.java13spring.java13springproject.libraryproject.dto;

import com.sber.java13spring.java13springproject.libraryproject.model.Director;
import com.sber.java13spring.java13springproject.libraryproject.model.Film;
import com.sber.java13spring.java13springproject.libraryproject.model.Genre;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmDTO
      extends GenericDTO {
    
    private String filmTitle;
    private String publishDate;
    private Integer pageCount;
    private Integer amount;
    private String storagePlace;
    private String onlineCopyPath;
    private String publish;
    private String description;
    private Genre genre;
    private Set<Long> directorsIds;
    private boolean isDeleted;
    
    public FilmDTO(Film film) {
        FilmDTO filmDTO = new FilmDTO();
        //из entity делаем DTO
        filmDTO.setFilmTitle(film.getFilmTitle());
        filmDTO.setGenre(film.getGenre());
        filmDTO.setDescription(film.getDescription());
        filmDTO.setId(film.getId());
        filmDTO.setPageCount(film.getPageCount());
        filmDTO.setPublishDate(film.getPublishDate().toString());
        Set<Director> directors = film.getDirectors();
        Set<Long> directorIds = new HashSet<>();
        if (directors != null && directors.size() > 0) {
            directors.forEach(a -> directorIds.add(a.getId()));
        }
        filmDTO.setDirectorsIds(directorIds);
    }
}
