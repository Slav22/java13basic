package com.sber.java13spring.java13springproject.libraryproject.service;

import com.sber.java13spring.java13springproject.libraryproject.dto.FilmDTO;
import com.sber.java13spring.java13springproject.libraryproject.dto.OrderDTO;
import com.sber.java13spring.java13springproject.libraryproject.mapper.OrderMapper;
import com.sber.java13spring.java13springproject.libraryproject.model.Order;
import com.sber.java13spring.java13springproject.libraryproject.repository.OrderRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class OrderService
        extends GenericService<Order, OrderDTO> {

    private final FilmService filmService;
    private final OrderMapper orderMapper;
    private final OrderRepository orderRepository;


    protected OrderService(OrderRepository orderRepository,
                           OrderMapper orderMapper,
                           FilmService filmService) {
        super(orderRepository, orderMapper);
        this.filmService = filmService;
        this.orderMapper = orderMapper;
        this.orderRepository = orderRepository;
    }

    public Page<OrderDTO> listUserRentFilms(final Long id,
                                            final Pageable pageable) {
        Page<Order> objects = orderRepository.getOrderByUserId(id, pageable);
        List<OrderDTO> results = orderMapper.toDTOs(objects.getContent());
        return new PageImpl<>(results, pageable, objects.getTotalElements());
    }

    public OrderDTO rentFilm(OrderDTO rentFilmDTO) {
        FilmDTO filmDTO = filmService.getOne(rentFilmDTO.getFilmId());
        filmDTO.setAmount(filmDTO.getAmount() - 1);
        filmService.update(filmDTO);
        long rentPeriod = rentFilmDTO.getRentPeriod() != null ? rentFilmDTO.getRentPeriod() : 14L;
        rentFilmDTO.setRentDate(LocalDateTime.now());
        rentFilmDTO.setReturned(false);
        rentFilmDTO.setRentPeriod((int) rentPeriod);
        rentFilmDTO.setReturnDate(LocalDateTime.now().plusDays(rentPeriod));
        rentFilmDTO.setCreatedWhen(LocalDateTime.now());
        rentFilmDTO.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        return mapper.toDTO(repository.save(mapper.toEntity(rentFilmDTO)));
    }

    public void returnFilm(final Long id) {
        OrderDTO orderDTO = getOne(id);
        orderDTO.setReturned(true);
        orderDTO.setReturnDate(LocalDateTime.now());
        FilmDTO filmDTO = orderDTO.getFilmDTO();
        filmDTO.setAmount(filmDTO.getAmount() + 1);
        update(orderDTO);
        filmService.update(filmDTO);
    }
}
