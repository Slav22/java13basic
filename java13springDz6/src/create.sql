create table reader (
    id serial primary key ,
    firstName varchar(30) not null ,
    surname varchar(30) not null ,
    birthday date,
    phone varchar(12),
    email varchar(30),
    list_book varchar(100)
);
create table book (
    id serial primary key ,
    title varchar(50) not null ,
    author varchar(50)not null ,
    year_publisher varchar(4),
    quantity_page int
);
insert into book(title, author, year_publisher, quantity_page)
values ('Вий','Н.В. Гоголь','1962',500);
insert into book(title, author, year_publisher, quantity_page)
values ('Вечера на хуторе близ Диканьки','Н.В. Гоголь','1992',550);
insert into book(title, author, year_publisher, quantity_page)
values ('Мастер и Маргарита','М.А. Булгаков','1985',450);

drop table  reader;
insert into  reader (firstName, surName, birthDay, phone, email, list_book)
values ('Вячеслав','Пономарев','1962-07-22','79511111111','vgponomarev62@mail.ru','{"Война и мир","Айвенго"}');
insert into  reader (firstName, surName, birthDay, phone, email, list_book)
values ('Сергей','Сергеев','2002-07-22','79513333333','sergeev@mail.ru','Вий');
select * from reader ;
select list_book from reader where phone = '79511111111';
select * from reader where phone = '79511111111';
select list_book from reader where phone ='79512222222';
select  * from book where title = 'Вий';
