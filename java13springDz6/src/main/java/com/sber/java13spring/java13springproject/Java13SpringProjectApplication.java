package com.sber.java13spring.java13springproject;

import com.sber.java13spring.java13springproject.dbexample.dao.BookDao;
import com.sber.java13spring.java13springproject.dbexample.dao.UserDAO;
import com.sber.java13spring.java13springproject.dbexample.dao.UserDaoBean;
import com.sber.java13spring.java13springproject.dbexample.model.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class Java13SpringProjectApplication
        implements CommandLineRunner {
    private UserDaoBean userDaoBean;
    private BookDao bookDao;

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public Java13SpringProjectApplication(UserDaoBean userDaoBean, BookDao bookDao, NamedParameterJdbcTemplate jdbcTemplate) {
        this.userDaoBean = userDaoBean;
        this.jdbcTemplate = jdbcTemplate;
        this.bookDao = bookDao;

    }

    public static void main(String[] args) {
        SpringApplication.run(Java13SpringProjectApplication.class, args);
    }




    @Override
    public void run(String... args) throws Exception {
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++" );
        System.out.println(bookDao.findUsersListBook(String.valueOf(userDaoBean.findUserByEmail("sergeev@mail.ru").getListBook())));


        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++" );
 //       userDaoBean.singUpUser("Петр", "Петров", "2000-02-22" ,
 //              "petrov@mail.ru", "79514444444", "Вий, Вечера на хуторе близ Диканьки" );
        List<User> user = jdbcTemplate.query("select * from reader",
                (rs, rowNum) -> new User(
                        rs.getInt("id" ),
                        rs.getString("firstName" ),
                        rs.getString("surName" ),
                        rs.getString("birthDay" ),
                        rs.getString("email" ),
                        rs.getString("phone" ),
                        rs.getString("list_book" )

                ));
        user.forEach(System.out::println);
    }
}
