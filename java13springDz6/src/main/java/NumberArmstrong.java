/*Доп1
        Напишите программу для проверки, является ли введение число - числом Армстронга
        Число Армстронга — натуральное число, которое в данной системе счисления равно сумме
        своих цифр, возведенных в степень, равную количеству его цифр. Иногда, чтобы считать
        число таковым, достаточно, чтобы степени, в которые возводятся цифры, были равны m.
        Например, десятичное число
        153— число Армстронга, потому что
        1^3 + 5^3 + 3^3 = 153.

 */

public class NumberArmstrong {
    public static void main(String[] args) {
        System.out.println(searchNextNumberArmstrong(10));
    }

    public static int searchNextNumberArmstrong(int x) {
        int result = x + 1;
        while (!numberIsArmstrong(result)) {
            result++;
        }
        return result;
    }

    public static boolean numberIsArmstrong(int number) {
        int result = 0;
        int temp = number;
        int countDigits = (int) Math.log10(number) + 1;
        while (temp != 0) {
            result += Math.pow(temp % 10, countDigits);
            temp /= 10;
        }
        return number == result;
    }
}